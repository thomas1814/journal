#! /usr/bin/env/ python3
import os
import shutil

# Create a desktop icon
wd = os.path.dirname(os.path.abspath(__file__))
filename = wd + "/Journal.desktop"
# Creats a new file.
if os.path.isfile(filename):
    os.remove(filename)

with open("Journal.desktop", "w+") as f:
    data = f.read()

    title = "[Desktop Entry]\n"
    version = "Version=0.1.0\n"
    name = "Name=Journal\n"
    generic_name = "GenericName=Journal\n"
    comment = "Comment=Journal application version 0.1.0\n"
    script = "Exec=" + wd + "/src/app.py\n"
    t = "Type=Application\n"
    terminal = "Terminal=false\n"
    path = "Path=" + wd + "/src/\n"
    icon = "Icon=" + wd + "icon_large.png\n"
    category = "Categories= Productivity;Editor\n"

    data = title + version + name + generic_name + comment + script + t + terminal + path + icon + category
    f.write(data)

# Give the desktop icon premission to run the application
cmd = "chmod +x " + wd + "/src/app.py"
os.system(cmd)

# Make a coppy of the desktop file in the application folder
expanduser = os.path.expanduser("~")
dst = expanduser + "/.local/share/applications/Journal.desktop"
shutil.copyfile(src=filename, dst=dst)
