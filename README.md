# README
A simple journal application written in python3 with a tkinter user interface. This project is developed on a system running Antergos, but also tested to work on MacOS and Windows.  

Known issues:  
* TK on macOS has issues with scrolling (trackpad) causing a crash and flickering on screen. The canvas has scrolling issues on macOS.  
* There are bugs (for example are title not clickable); Contributions (PR/MR) are welcome.  
* It is not finished yet, so some parts, like drop-down menus (symbolized with =), are not yet completed (hint: use arrow up and down keys).  


## Binary Files  
Binary file can be downloaded from:  
https://www.dropbox.com/sh/qxxuudyozy751ld/AAAnh1h4Rz1J6nAD22-f0wISa?dl=0  

![](gallery1.png)
![](gallery2.png)
![](gallery3.png)
![](gallery4.png)
![](gallery5.png)
![](gallery6.png)

## Installation from Source on Antergos  
Requirements  
* Python 3.6.3: ``$ sudo pacman -S python``  
* tk 8.5.18: ``$ sudo pacman -S tk``  
* Pillow 4.2.1: ``$ sudo pacman -S python-pillow``  

``$ git clone git@gitlab.com:thomas1814/journal.git``   
``$ cd path/to/journal/  
$ python setup.py``  
The setup script creates a desktop application (only on Linux) and it can therefor be started as a regular desktop application. Running the application from terminal:  
``$ cd path/to/journal/src/  
$ python app.py``  

## Compiling a Single Executable File on Antergos:  
Requirements:  
* pip 9.0.1-3 ``$ sudo pacman -S python-pip``  
* virtualenv 15.1.0-2 ``$ sudo pacman -S python-virtualenv``  
* Pillow ``$ pip install Pillow `` # Pyinstaller required this version from pip to work for some reason  
* PyInstaller 3.3 (PyInstaller 3.2 does not support Python3.6) ``$ pip install pyinstaller``  

Create and setup a virtual environment  
``$ mkdir ~/virtualenv  
$ virtualenv -p python3.6 ~/virtualenv/jve``  
Activate development:  
``$ source ~/virtualenv/jve/bin/activate`` # Remember to deactivate  

Building using pyinstaller (activate virtualenv first!):  
``$ cd ~/gitlab/journal/src/``  
``$ pyinstaller --onefile -w --hidden-import=Pillow --hidden-import=Tkinter --hidden-import=PIL._tkinter_finder --add-data '../resources/':'resources/' --add-data '../LICENSE':'./' app.py --clean --noconfirm --name Journal``  

A binary file is created in the dist/Journal/ folder. The executable can be run by double clicking it or in a terminal ``$ ./Journal``  


## Installation from Source on MacOS  
Requirements  
* Python 3.6.3: ``$ brew install python3``  
* tk 8.5.18: ``brew install tcl-tk``  
* pip3 9.0.1-3: Comes installed with Python3.  
* Pillow 4.2.1: ``$ pip3 install Pillow``  

``$ git clone git@gitlab.com:thomas1814/journal.git``   
``$ cd path/to/journal/``  

Create and setup a virtual environment  
``$ mkdir ~/virtualenv  
$ virtualenv -p python3.6 ~/virtualenv/jve``  
Activate development:  
``$ source ~/virtualenv/jve/bin/activate`` # Remember to deactivate  
Install Pillow:  
``$ pip3 install Pillow``  

Running the application from terminal (in virtualenv):  
``$ cd path/to/journal/src/  
$ python3 app.py``  


## Compiling a Single Executable File on MacOS:  

Install pyinstaller (remember to activate virtualenv first!):
* PyInstaller 3.3 (PyInstaller 3.2 does not support Python3.6) ``$ pip3 install pyinstaller``  

Building using pyinstaller (activate virtualenv first!):  
``$ cd ~/gitlab/journal/src/``  
``$ pyinstaller --onefile -w --noupx --hidden-import=Pillow --hidden-import=Tkinter --hidden-import=PIL._tkinter_finder  --add-data  "../resources/:resources/" --add-data "../LICENSE:./" --clean --noconfirm --name Journal --osx-bundle-identifier Journal app.py ``  


## Installation from Source on Windows:  
Install Chocolate Package Manager in PowerShell as administrator:  
``$ Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))``  

Install Git:  
``$ choco install git -y  
$ [Environment]::SetEnvironmentVariable("Path", "$env:Path;C:\Program Files\Git", "User")``  

Install Python 3.6.3:  
``$ choco install python -y  
$ [Environment]::SetEnvironmentVariable("Path", "$env:Path;C:\Python36", "User")``  

(reopen powershell to use python)  

pip 9.0.1 (installed with python)  
tkinter 8.6 (installed with python)  

Install virtualenv-15.1.0:  
``$ Set-ExecutionPolicy Unrestricted  
$ pip install virtualenv``  
Activate virtualenv:  
``$ mkdir virtualenv/  
$ virtualenv virtualenv/jve  
$ virtualenv/jve/Scripts/activate ``  

`` $ pip install Pillow  
$ pip install Pyinstaller ``  

``$ git clone https://gitlab.com/thomas1814/journal.git``   
``$ cd path/to/journal/``  

Running the application from terminal (in virtualenv):  
``$ cd path/to/journal/src/  
$ python3 app.py``  


## Compiling a Single Executable File on Windows:  
Install pyinstaller (remember to activate virtualenv first!):
* PyInstaller 3.3 (PyInstaller 3.2 does not support Python3.6) ``$ pip install pyinstaller``  

Building using pyinstaller (activate virtualenv first!):  
``$ pyinstaller --onefile -w --hidden-import=Pillow --hidden-import=Tkinter --hidden-import=PIL._tkinter_finder --add-data  “../resources/;resources/” --add-data “../LICENSE;./” --clean --noconfirm --name Journal app.py``  
