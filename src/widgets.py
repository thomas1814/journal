#!/usr/bin/env python3
import datetime
import calendar
import sys
from tkinter import Tk, font, Label, Frame, StringVar, Checkbutton, IntVar, \
    END, Entry, Text, WORD, DISABLED, NORMAL, Canvas, CENTER, Button

"""
This module contains custom made tkinter widgets.

The widgets in this module are widgets that are usfull, but not present in
standard tkinter. This module has no dependencies except standard Python 3.
The Theme class is used for themeing the widgets.

WIDGETS:
* tButton
* tLink
* TextField
* TextBox
* DatePicker
* TimePicker
* Selector
* DeleteButton
* CardHolder
* Card
"""

__author__ = "Thomas Rostrup Andersen"
__copyright__ = """Copyright (C) 2017, Thomas Rostrup Andersen.
                   All rights reserved."""
__license__ = "BSD 2-clause"
__version__ = "0.1.8"
__all__ = ["Theme", "tButton", "tLink", "TextField", "TextBox",
        "SelectPasswordBox", "RetypePasswordBox", "SelectUsernameBox",
        "DatePicker", "TimePicker", "Selector", "DeleteButton", "CardHolder",
        "Card"]


class Theme():
    """Theme class for custom tkinter widgets.

    The themes in this class are implemented as static member variables. The
    class contains selected color and fonts for theming.

    USAGE:
        Theme.set_dark_theme()
        color = Theme.background_color  # a dark color
        Theme.set_light_theme()
        color = Theme.background_color  # a light color
    """

    # Used my the Theme class for internal testing
    _system_fonts = []
    _class_font = 0

    # Used for widget themeing
    window_background = None
    card_background = None
    title_color = None
    title_font = None
    text_color = None
    text_font = None
    highlight_color = None
    subscript_font = None
    subscript_color = None
    danger_color = None
    warning_color = None
    good_color = None
    gray_color = None
    hint_text_color = None
    link_color = None

    headline_color = None

    button_background = None
    button_border = None
    button_font = None
    button_color = None
    button_text_color = None
    button_hover_color = None
    button_hover_text_color = None
    button_hover_border = None
    button_pressed_color = None
    button_pressed_text_color = None
    button_pressed_border = None
    button_focused_color = None
    button_focused_text_color = None
    button_focused_border = None
    button_disabled_color = None
    button_disabled_text_color = None
    button_disabled_border = None
    button_selected_color = None
    button_selected_text_color = None
    button_selected_border = None

    @classmethod
    def set_dark_theme(cls):
        cls.window_background = "#424242"
        cls.card_background = "#333333"
        cls.title_color = "#b7b7b7"
        cls.title_font = ("terminus", 10, "bold")
        cls.text_color = "#b7b7b7"
        cls.text_font = ("terminus", 10)  # ("mtx", 12)
        cls.highlight_color = "#FFFFFF"
        cls.subscript_font = ("terminus", 8, "italic")
        cls.subscript_color = "#999999"
        cls.danger_color = "#8e2a1f"
        cls.warning_color = "#d8b35b"
        cls.good_color = "#6ea065"
        cls.gray_color = "#555655"
        cls.border_color = "#2b2929"
        cls.hint_text_color = "#6b6b6b"
        cls.link_color = "#2fcec1"
        cls.headline_color = "#397cce"
        cls.headline_font = ("terminus", 14, "bold")
        cls.button_background = cls.window_background
        cls.button_border = cls.border_color
        cls.button_font = ("terminus", 10)
        cls.button_color = "#4f5154"
        cls.button_text_color = cls.text_color
        cls.button_hover_color = "#6d6d6d"
        cls.button_hover_text_color = cls.text_color
        cls.button_hover_border = cls.button_border
        cls.button_pressed_color = cls.card_background
        cls.button_pressed_text_color = cls.text_color
        cls.button_pressed_border = cls.button_border
        cls.button_focused_color = cls.button_hover_color
        cls.button_focused_text_color = cls.text_color
        cls.button_focused_border = cls.button_border
        cls.button_disabled_color = cls.card_background
        cls.button_disabled_text_color = cls.hint_text_color
        cls.button_disabled_border = cls.button_border
        cls.button_selected_color = cls.button_disabled_color
        cls.button_selected_text_color = cls.button_disabled_text_color
        cls.button_selected_border = cls.button_disabled_border

    @classmethod
    def set_ligth_theme(cls):
        cls.window_background = "#f2f2f2"
        cls.card_background = "#e5e5e5"
        cls.title_color = "#63625f"
        cls.title_font = ("mtx", 12, "bold")
        cls.text_color = "#63625f"
        cls.text_font = ("mtx", 12)
        cls.highlight_color = "#FFFFFF"
        cls.subscript_font = "Helvetica 10 italic"
        cls.subscript_color = "#999999"
        cls.danger_color = "#8e2a1f"
        cls.warning_color = "#d8b35b"
        cls.good_color = "#6ea065"
        cls.gray_color = "#555655"
        cls.border_color = "#c4c4c4"
        cls.hint_text_color = "#9b9b9b"

        cls.button_background = cls.window_background
        cls.button_border = cls.border_color
        cls.button_font = ("mtx", 10)
        cls.button_color = "#e5e5e5"
        cls.button_text_color = cls.text_color
        cls.button_hover_color = cls.button_color
        cls.button_hover_text_color = "#000000"
        cls.button_hover_border = cls.button_border
        cls.button_pressed_color = cls.window_background
        cls.button_pressed_text_color = "#ffffff"
        cls.button_pressed_border = cls.button_border
        cls.button_focused_color = cls.button_hover_color
        cls.button_focused_text_color = cls.text_color
        cls.button_focused_border = cls.button_border
        cls.button_disabled_color = cls.card_background
        cls.button_disabled_text_color = cls.hint_text_color
        cls.button_disabled_border = cls.button_border

    @classmethod
    def set_sience_theme(cls):
        cls.window_background = "#424242"
        cls.card_background = "#333333"
        cls.title_color = "#b7b7b7"
        cls.title_font = "\"mtx\" 12"
        cls.text_color = "#1ca0b5"
        cls.text_font = ("mtx", 12)
        cls.highlight_color = "#FFFFFF"
        cls.subscript_font = "Helvetica 10 italic"
        cls.subscript_color = "#999999"
        cls.danger_color = "#8e2a1f"
        cls.warning_color = "#d8b35b"
        cls.good_color = "#6ea065"
        cls.gray_color = "#555655"
        cls.border_color = "#2b2929"
        cls.hint_text_color = "#6b6b6b"

    @classmethod
    def list_fonts(cls):
        """Print all the fonts available in the current operating system."""
        root = Tk()
        fonts = font.families()
        print("---------------Fonts (", len(fonts), ")--------------------")
        for f in fonts:
            print(f)
        print("------------------------------")
        root.destroy()

    @classmethod
    def font_viewer(cls):
        """View all the fonts available in the current operating system."""
        root = Tk()
        root.geometry("800x600")
        cls._system_fonts = font.families()
        for t in cls._system_fonts:
            print(t)

        root.columnconfigure(0, weight=1)
        root.columnconfigure(1, weight=1)
        root.rowconfigure(2, weight=1)
        cls._label = Label(root, text="Default")
        cls._label.grid(row=0, column=0, columnspan=2, rowspan=1,
                        sticky="wens", padx=(100, 100), pady=(100, 100))
        cls._next_button = Button(root, text="Next", command=cls._next_font)
        cls._next_button.grid(row=2, column=1, columnspan=1, rowspan=1,
                              sticky="wes", padx=(100, 100), pady=(100, 100))
        cls._back_button = Button(root, text="Back", command=cls._back_font)
        cls._back_button.grid(row=2, column=0, columnspan=1, rowspan=1,
                              sticky="wes", padx=(100, 100), pady=(100, 100))

    @classmethod
    def _next_font(cls):
        try:
            cls._class_font = cls._class_font + 1
        except Exception as e:
            cls._class_font = 0
            print(e)
        font = cls._system_fonts[cls._class_font]
        cls._label.configure(text=font, font=(font, 12))

    @classmethod
    def _back_font(cls):
        try:
            cls._class_font = cls._class_font - 1
        except Exception as e:
            cls._class_font = len(cls._system_fonts) - 1
            print(e)
        font = cls._system_fonts[cls._class_font]
        cls._label.configure(text=font, font=(font, 12))


class tButton(Frame):

    """A button widget.

    SUPPORTS:
        * Callback when cliked or focused and enter is pressed.
        * Themed by Theme.

    USAGE:
        def func():
            print("Hello, world!")
        # frame is the tkinter Frame where the widget is to be displayed
        frame = Tk()
        button = tButton(parent=frame, label="Cancel", callback=func)
        button.pack()  # tkinter displays the widget on frame
        frame.mainloop()
        # Note the mainloop abow blocks
    """

    def __init__(self, parent, label="ok", image=None, callback=None, *args,
                 **kwargs):
        """Construct the text field.

        parent - The widget parent where it should be displayed.
        label - Text to display on the button.
        image - An image/icon to use on the button.
        callback - A function that is called every time the text field is
                edited.
        *args, **kwargs - Passed to tkinter Frame (see tkinter documentation).
        """
        self.parent = parent
        self.label = label
        self.callback = callback
        super().__init__(parent, *args, **kwargs)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self._disabled = False
        self.pressed = False
        self.hovering = False
        self.focused = False
        self._selected = False
        self.image = image
        self.image_pressed = None
        self.image_hovering = None
        self.image_focused = None
        self.image_selected = None
        self.image_disabled = None

        space = 10
        border = 1
        self._border = Frame(self)
        self._border.grid(row=0, column=0, columnspan=1, rowspan=1,
                          sticky="wens")
        self._border.columnconfigure(0, weight=1)
        self._border.rowconfigure(0, weight=1)
        self._space = Frame(self._border)
        self._space.grid(row=0, column=0, columnspan=1, rowspan=1,
                         sticky="wens", padx=(border, border),
                         pady=(border, border))
        self._space.columnconfigure(0, weight=1)
        self._space.rowconfigure(0, weight=1)

        self._label_value = StringVar()
        self._label_value.set(label)
        # if image is not None:
        self._label = Label(self._space, textvariable=self._label_value,
                            image=self.image, compound="left",
                            anchor=CENTER, borderwidth=0, takefocus=1)
        self._label.grid(row=0, column=0, columnspan=1, rowspan=1,
                 sticky="wens", padx=(space, space), pady=(5, 4))

        self._set_state()
        self._set_image()

        # Bind events
        self.bind("<ButtonRelease-1>", self._clicked)  # cliked
        self.bind("<Enter>", self._hover)  # hover
        self.bind("<Leave>", self._leave)  # leave hovering
        self.bind("<Button-1>", self._press)  # press
        self.bind("<FocusIn>", self._focus_in)  # focus in
        self.bind("<FocusOut>", self._focus_out)  # focus out

    def bind(self, event, function):
        """Bind an event to the button (see tkinter documentation)."""
        super().bind(event, function)
        self._border.bind(event, function)
        self._space.bind(event, function)
        self._label.bind(event, function)

    def unbind(self, event):
        """Unbind an event from the button (see tkinter documentation)."""
        super().unbind(event)
        self._border.unbind(event)
        self._space.unbind(event)
        self._label.unbind(event)

    @property
    def disabled(self):
        """Returns True if button is disabled."""
        return self._disabled

    @disabled.setter
    def disabled(self, value):
        """Disable the button by setting it as False.

        Enable it by setting it as True."""
        self._disabled = value
        self._set_state()
        self._set_image()

    @property
    def selected(self):
        """Return True if button is selected."""
        return self._selected

    @selected.setter
    def selected(self, value):
        """Set the state as selected (True) or deselected (False)."""
        self._selected = value
        self.disabled = True if self.selected else False

    def _clicked(self, event):
        """Call the callback function and set state (_set_state())."""
        self._set_state()
        self._set_image()
        if (self.hovering or self.focused) and not self.disabled:
            if self.callback is not None:
                self.callback()
        self.pressed = False

    def _hover(self, event):
        """Set hovering as True and set the state (_set_state())."""
        self.hovering = True
        self._set_state()
        self._set_image()

    def _leave(self, event):
        """Set hovering as False and set the state (_set_state())."""
        self.hovering = False
        self._set_state()
        self._set_image()

    def _press(self, event):
        """Set pressed as False and set the state (_set_state())."""
        self.pressed = True
        self._set_state()
        self._set_image()

    def _focus_in(self, event):
        """Bind the return key, set focused as True and set the state
        (_set_state())."""
        self.bind("<Return>", self._clicked)
        self.focused = True
        self._set_state()
        self._set_image()

    def _focus_out(self, event):
        """Unbind the return key, set focused as False and set the state
        (_set_state())."""
        self.unbind("<Return>")
        self.focused = False
        self._set_state()
        self._set_image()

    def _return_pressed(self, event):
        """Call the callback function and and set the state (_set_state())."""
        if self.callback is not None and not self.disabled:
            self.callback()
        self._set_state()
        self._set_image()

    def _set_image(self):
        """Set the image/icon according to state and images provided."""
        self.configure(background=Theme.button_background)
        if self.selected and self.image_selected is not None:
            self._label.configure(image=self.image_selected)
        elif self.disabled and self.image_disabled is not None:
            self._label.configure(image=self.image_disabled)
        elif self.pressed and self.image_pressed is not None:
            self._label.configure(image=self.image_pressed)
        elif self.hovering and self.image_hovering is not None:
            self._label.configure(image=self.image_hovering)
        elif self.focused and self.image_focused is not None:
            self._label.configure(image=self.image_focused)
        elif self.image is not None:
            self._label.configure(image=self.image)

    def _set_state(self):
        """Set the state (configure the apprence) of the button.

        The apprence is set based on self.disabled, self.pressed,
        self.hovering and self.focused (True/False). This function can be
        overloaded to change apprence.

        EXAMPLE:
            if self.selected:
                pass
            elif self.disabled:
                pass
            elif self.pressed:
                self._border.configure(background=Theme.button_pressed_border)
                self._label.configure(background=Theme.button_pressed_color)
                self._space.configure(background=Theme.button_pressed_color)
            elif self.hovering:
                pass
            elif self.focused:
                pass
            else:
                pass
        """
        self.configure(background=Theme.button_background)
        if self.selected:
            self._border.configure(background=Theme.button_selected_border)
            self._label.configure(background=Theme.button_selected_color,
                                  foreground=Theme.button_selected_text_color,
                                  font=Theme.button_font)
            self._space.configure(background=Theme.button_selected_color)
        elif self.disabled:
            self._border.configure(background=Theme.button_disabled_border)
            self._label.configure(background=Theme.button_disabled_color,
                                  foreground=Theme.button_disabled_text_color,
                                  font=Theme.button_font)
            self._space.configure(background=Theme.button_pressed_color)
        elif self.pressed:
            self._border.configure(background=Theme.button_pressed_border)
            self._label.configure(background=Theme.button_pressed_color,
                                  foreground=Theme.button_pressed_text_color,
                                  font=Theme.button_font)
            self._space.configure(background=Theme.button_pressed_color)
        elif self.hovering:
            self._border.configure(background=Theme.button_hover_border)
            self._label.configure(background=Theme.button_hover_color,
                                  foreground=Theme.button_hover_text_color,
                                  font=Theme.button_font)
            self._space.configure(background=Theme.button_hover_color)
        elif self.focused:
            self._border.configure(background=Theme.button_pressed_border)
            self._label.configure(background=Theme.button_focused_color,
                                  foreground=Theme.button_focused_text_color,
                                  font=Theme.button_font)
            self._space.configure(background=Theme.button_focused_color)
        else:
            self._border.configure(background=Theme.button_border)
            self._label.configure(background=Theme.button_color,
                                  foreground=Theme.button_text_color,
                                  font=Theme.button_font)
            self._space.configure(background=Theme.button_color)


class tLink(tButton):

        def _set_state(self):
            """Set the state (configure the apprence) of the button.

            The apprence is set based on self.disabled, self.pressed,
            self.hovering and self.focused (True/False). This function can be
            overloaded to change apprence.

            EXAMPLE:
                if self.selected:
                    pass
                elif self.disabled:
                    pass
                elif self.pressed:
                    self._border.configure()
                    self._label.configure()
                    self._space.configure()
                elif self.hovering:
                    pass
                elif self.focused:
                    pass
                else:
                    pass
            """
            self.configure(background=Theme.window_background)
            if self.selected:
                self._border.configure(background=Theme.window_background)
                self._label.configure(background=Theme.window_background,
                                foreground=Theme.button_selected_text_color,
                                      font=Theme.button_font)
                self._space.configure(background=Theme.window_background)
            elif self.disabled:
                self._border.configure(background=Theme.window_background)
                self._label.configure(background=Theme.window_background,
                                foreground=Theme.button_disabled_text_color,
                                      font=Theme.button_font)
                self._space.configure(background=Theme.window_background)
            elif self.pressed:
                self._border.configure(background=Theme.window_background)
                self._label.configure(background=Theme.window_background,
                                      foreground=Theme.card_background,
                                      font=Theme.button_font)
                self._space.configure(background=Theme.window_background)
            elif self.hovering:
                self._border.configure(background=Theme.window_background)
                self._label.configure(background=Theme.window_background,
                                      foreground=Theme.warning_color,
                                      font=Theme.button_font)
                self._space.configure(background=Theme.window_background)
            elif self.focused:
                self._border.configure(background=Theme.window_background)
                self._label.configure(background=Theme.window_background,
                                      foreground=Theme.warning_color,
                                      font=Theme.button_font)
                self._space.configure(background=Theme.window_background)
            else:
                self._border.configure(background=Theme.window_background)
                self._label.configure(background=Theme.window_background,
                                      foreground=Theme.button_text_color,
                                      font=Theme.button_font)
                self._space.configure(background=Theme.window_background)


class TextField(Frame):

    """A text field widget that can display and edit text (multiline).

    SUPPORTS:
        * Hints.
        * Callback when edited.
        * Restrict symbols allowed as input.

    USAGE:
        # frame is the tkinter Frame where the widget is to be displayed
        frame = Tk()
        text_display = TextField(parent=frame, hint="What?")
        text_display.pack()  # tkinter displays the widget on frame
        frame.mainloop()

        # Note the mainloop abow blocks
        # To get and set the value of the textfield use the .value property.
        text_value.value = "Something"
        print(text_display.value)  # Prints the text display value
    """

    def __init__(self, parent, hint="Hint...", symbols=None, callback=None,
                 card=False, boxed=False, width=None, height=None,
                 editable=True, scrollbar=True, *args, **kwargs):
        """Construct the text field.

        parent - The widget parent where it should be displayed.
        hint - Displays a hint for the text field. Disappears when users starts
            typing.
        symbols - Contains a string of all symbols that are allowed in the text
               field. The hint is excepted from the symbol list. If the
               value property is used, all the symbols in the input must be
               in the symbol list, else the old value will remain.
        callback - A function that is called every time the text field is
                edited.
        card - True indicate that the textfield is on a card.
        boxed - If True, create a boxed version.
        width - Width of text field. Default let tkinter select width.
        height - Height of text field. Default let tkinter select height.
        editable - If text is editable by the user. The value property is
                excluded.
        scrollbar - Show scrollbar.
        *args, **kwargs - Passed to tkinter Frame (see tkinter documentation).
        """
        self.hint = hint
        self.parent = parent
        self.symbols = symbols
        self.callback = callback
        super().__init__(parent, *args, **kwargs)

        pad = 0
        space = 2 if boxed else 0
        border = 1 if boxed else 0
        self._border = Frame(self)
        self._border.grid(row=0, column=0, columnspan=1, rowspan=1,
                          sticky="wens")
        self._border.columnconfigure(0, weight=1)
        self._border.rowconfigure(0, weight=1)
        self._space = Frame(self._border)
        self._space.grid(row=0, column=0, columnspan=1, rowspan=1,
                         sticky="wens", padx=(border, border),
                         pady=(border, border))
        self._space.columnconfigure(0, weight=1)
        self._space.rowconfigure(0, weight=1)

        self._header_value = StringVar()
        self._header_value.set("")
        self._footer_value = StringVar()
        self._footer_value.set("")

        self._header = Label(self._space, textvariable=self._header_value,
                            borderwidth=0)
        self._header.grid(row=1, column=1, columnspan=1, rowspan=1,
                         sticky="en", padx=(pad, pad), pady=(pad, pad))
        self._footer = Label(self._space, textvariable=self._footer_value,
                            borderwidth=0)
        self._footer.grid(row=1, column=1, columnspan=1, rowspan=1,
                         sticky="es", padx=(pad, pad), pady=(pad, pad))

        self._text = Text(self._space, wrap=WORD)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        self._text.grid(row=1, column=0, columnspan=1, rowspan=1,
                        sticky="wens", padx=(space, space),
                        pady=(space, space))
        if width is not None:
            self._text.configure(width=width)
        if height is not None:
            self._text.configure(height=height)
        self.editable = editable
        self.scrollbar = scrollbar

        # Configure the GUI
        background = Theme.card_background if card else Theme.window_background
        self.configure(background=background)
        self._border.configure(background=Theme.border_color)
        self._space.configure(background=background)
        self._header.configure(background=background,
                              foreground=Theme.hint_text_color,
                              font=(Theme.text_font[0], 7))
        self._footer.configure(background=background,
                              foreground=Theme.hint_text_color,
                              font=(Theme.text_font[0], 7))
        self._text.configure(background=background,
                             foreground=Theme.text_color,
                             font=Theme.text_font, borderwidth=0,
                             highlightthickness=0,
                             insertbackground=Theme.text_color)

        # Bind events
        self._text.bind("<KeyRelease>", self._change)
        self._text.bind("<Button-4>", self._update_labels)  # Mouse wheel up
        self._text.bind("<Button-5>", self._update_labels)  # Mouse wheel down
        self._text.bind("<MouseWheel>", self._update_labels)  # None Linux
        self._text.bind("<Prior>", self._update_labels)  # Page up
        self._text.bind("<Next>", self._update_labels)  # Page down
        self.bind('<FocusIn>', self._focus_in)
        self.bind('<FocusOut>', self._focus_out)
        self.bind('<ButtonRelease-1>', self._mouse)  # Mouse left click
        self.bind('<Button-1>', self._mouse)  # Mouse left click

        self._update_labels(event="event")

    def _change(self, event):
        """Update the labels and call the callback function."""
        self._update_labels(event)
        if self.callback is not None:
            self.callback()

    def _update_labels(self, event):
        """Update the scrollbar labels according to the position."""
        if self.scrollbar:
            c = self._text.yview()
            y = int(c[0] * 100)
            y = "{}%".format(y)
            self._header_value.set(y)
            y = int(c[1] * 100)
            y = "{}%".format(y)
            self._footer_value.set(y)
        else:
            self._text.yview_moveto(fraction=0)
            self.update()

    def bind(self, event, function):
        super().bind(event, function)
        self._border.bind(event, function)
        self._space.bind(event, function)
        self._text.bind(event, function)
        self._header.bind(event, function)
        self._footer.bind(event, function)

    @property
    def value(self):
        """Return the text field value."""
        value = self._text.get("0.0", 'end-1c')
        return value if value != "" and value != self.hint else ""

    @value.setter
    def value(self, value):
        """Set the text field value to value and update the display."""
        self._text.configure(state=NORMAL)  # Allows editing
        self._text.delete(0.0, END)
        if value == "" or value is None:
            self._text.insert(0.0, self.hint)
            self._text.configure(foreground=Theme.hint_text_color)
        else:
            self._text.insert(0.0, value)
            self._text.configure(foreground=Theme.text_color)

        self._text.yview_moveto(fraction=0)
        self.update()  # Needed because if not yview returns wrong data.
        self._update_labels(event="event")
        self.update()
        self.editable = self.editable  # Set state back to original state.

    @property
    def editable(self):
        """Return True if text field is editable."""
        return self._editable

    @editable.setter
    def editable(self, editable):
        """Set the text field as editable (True) or not (False) by the user."""
        self._editable = editable
        if self.editable:
            self._text.configure(state=NORMAL)
        else:
            self._text.configure(state=DISABLED)
        self.update()

    @property
    def scrollbar(self):
        """Return True if text field is scrollable."""
        return self._scrollbar

    @scrollbar.setter
    def scrollbar(self, scrollbar):
        """Set scrollbar on the text field (True) or not (False)."""
        self._scrollbar = scrollbar
        if self.scrollbar:
            self._update_labels(event="event")
        else:
            self._header_value.set("")
            self._footer_value.set("")

    def _focus_in(self, event):
        """Set the cursor to index 0 and bind key press and release events."""
        if self._text.get("0.0", 'end-1c') == self.hint:
            self._text.mark_set("insert", 0.0)
        self._text.bind('<Key>', self._key_press)
        self._text.bind('<KeyRelease>', self._key_release)

    def _focus_out(self, event):
        """Set value to hint if the value is empty."""
        if self._text.get("0.0", 'end-1c') == "":
            self._text.delete(0.0, END)
            self._text.insert(0.0, self.hint)
        if self._text.get("0.0", 'end-1c') == self.hint:
            self._text.configure(foreground=Theme.hint_text_color)
            self.update()

    def _key_press(self, event):
        """If value is hint (nothing written yet), remove the hint."""
        if self._text.get("0.0", 'end-1c') == self.hint:
            self._text.delete(0.0, END)
            self._text.insert(0.0, "")
            self._text.mark_set("insert", 0.0)

    def _key_release(self, event):
        """Set text color, call callback function and set text.

        If the new value is empty or same as hint, set it to hint and change
        color to Theme.hint_text_color, else set to Theme.text_color and use
        self.show. Call the callback if it exitsts."""
        value = self._text.get("0.0", 'end-1c')
        if value == "" or value == self.hint:
            self._text.delete(0.0, END)
            self._text.insert(0.0, self.hint)
            self._text.mark_set("insert", 0.0)
            self._text.configure(foreground=Theme.hint_text_color)
        else:
            self._text.configure(foreground=Theme.text_color)
        self.update()
        if self.callback is not None:
            self.callback()

    def _mouse(self, event):
        """Set cursor index to 0 and deselect text."""
        if self._text.get("0.0", 'end-1c') == self.hint:
            self._text.mark_set("insert", 0.0)


class TextBox(Frame):

    """A text field widget that can display and edit text.

    SUPPORTS:
        * Hints.
        * Displaying of symbols to hide text value.
        * Callback when edited.
        * Restrict symbols allowed as input.

    USAGE:
        # frame is the tkinter Frame where the widget is to be displayed
        frame = Tk()
        text_field = TextBox(parent=frame, hint="First name...")
        text_field.pack()  # tkinter displays the widget on frame
        frame.mainloop()

        # Note the mainloop abow blocks
        # To get and set the value of the textfield use the .value property.
        text_field.value = "John"
        print(text_field.value)  # Prints the text field value
    """

    def __init__(self, parent, hint="Hint...", symbols=None, show="",
                 callback=None, card=False, boxed=False, font="text",
                 justify="left", width=None, editable=True, *args, **kwargs):
        """Construct the textfield.

        parent - The widget parent where it should be displayed.
        hint - Displays a hint for the text field. Disappears when users starts
               typing.
        symbols - Contains a string of all symbols that are allowed in the text
                  field. The hint is excepted from the symbol list. If the
                  value property is used, all the symbols in the input must be
                  in the symbol list, else the old value will remain.
        show - The textfield displays the show text string instead of its real
               value. For example show="*" can turn the textfield into a
               password field. The hint is excluded from this.
        callback - A function that is called every time the text field is
                   edited.
        card - True indicate that the textfield is on a card.
        boxed - If True, create a boxed version.
        font - Tell what type of font to use. Valid input is "title" or "text".
        justify - Text placement.
        width - The width of the text box.
        editable - If True, the user can edit the text.
        """
        self.hint = hint
        self.parent = parent
        self.symbols = symbols
        self.show = show
        self.callback = callback
        super().__init__(parent, *args, **kwargs)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

        if font == "title":
            font = Theme.title_font
        else:
            font = Theme.text_font

        # Set configuration values according to input
        border = 0
        space = 0
        background = Theme.window_background
        foreground = Theme.window_background
        if boxed:
            border = 1
            space = 4
            foreground = Theme.card_background
        if card:
            background = Theme.card_background
            foreground = Theme.card_background
        if boxed and card:
            background = Theme.card_background
            foreground = Theme.window_background
        self._border = Frame(self)
        self._border.grid(row=0, column=0, columnspan=1, rowspan=1,
                          sticky="wens")
        self._border.columnconfigure(0, weight=1)
        self._border.rowconfigure(0, weight=1)
        self._space = Frame(self._border)
        self._space.grid(row=0, column=0, columnspan=1, rowspan=1,
                         sticky="wens", padx=(border, border),
                         pady=(border, border))
        self._space.columnconfigure(0, weight=1)
        self._space.rowconfigure(0, weight=1)

        # Calculate the needed width if no width is spesified
        if width is None:
            width = len(self.hint) + 1
            # Python tkinter estimate space based on length of string and the
            # size of 0. Since 0 is often larger then other letters, a
            # shrinking is needed.
            # If a currier type of font is used, it should bobably be 1
            width = int(width * 0.85) if width > 10 else 10

        # The validator is a Tk checker to see if the incomming letter(s) are
        # allowed or not (see the self._validate function).
        validator = (self._space.register(self._validate), '%S', '%P',
                     '%s', "%d")
        self._text = Entry(self._space, validate='key',
                           validatecommand=validator, show="", justify=justify,
                           width=width, *args, **kwargs)
        space = 3 if boxed else 0  # padding from label to border
        self._text.grid(row=0, column=0, columnspan=1, rowspan=1,
                        sticky="wens", padx=(1, space),
                        pady=(space * 2, space))

        self.configure(background=background)
        self._border.configure(background=Theme.border_color)
        self._space.configure(background=foreground)
        self._text.configure(background=foreground, borderwidth=0,
                       foreground=Theme.hint_text_color, font=font,
                       highlightthickness=0, insertbackground=Theme.text_color,
                       disabledbackground=foreground,
                       disabledforeground=Theme.text_color)
        self._text.delete(0, last=END)
        self._text.insert(0, self.hint)
        self._text.bind('<FocusIn>', self._focus_in)
        self._text.bind('<FocusOut>', self._focus_out)
        self._text.bind('<ButtonRelease-1>', self._mouse)
        self._text.bind('<Button-1>', self._mouse)
        self.editable = editable

    @property
    def value(self):
        """Return the text value (return "" if hint is displayed)."""
        value = self._text.get()
        return value if value != "" and value != self.hint else ""

    @value.setter
    def value(self, value):
        """Set text to value."""
        self._text.configure(state=NORMAL)  # Allows editing
        self._text.delete(0, last=END)
        if value == "" or value is None or value == self.hint:
            self._text.insert(0, self.hint)
            self._text.configure(foreground=Theme.hint_text_color, show="")
        else:
            self._text.insert(0, value)
            self._text.configure(foreground=Theme.text_color, show=self.show)
        self.update()
        self.editable = self.editable  # Set state back to original state.

    @property
    def editable(self):
        """Return True if text box is editable."""
        return self._editable

    @editable.setter
    def editable(self, editable):
        """Set the text box as editable (True) or not (False) by the user."""
        self._editable = editable
        if self.editable:
            self._text.configure(state=NORMAL)
        else:
            self._text.configure(state=DISABLED)
        self.update()

    def _validate(self, char, next_value, current_value, insert):
        """Check if char contains a valid symbol.

        Return true if char is in self.symbol, it is a delete event or the
        next_value would be equal to the self._hint.
        """
        if insert == "0":  # The event is deleting, always allowed
            return 1
        if next_value == self.hint:  # self.value change, allowed
            return 1
        if self.symbols is not None:  # There are restrictions
            if char in self.symbols:
                return 1
            else:
                return 0
        else:
            return 1

    def _focus_in(self, event):
        """Set the cursor to index 0 and bind key press and release events."""
        if self._text.get() == self.hint:
            self._text.icursor(index=0)
        self._text.bind('<Key>', self._key_press)
        self._text.bind('<KeyRelease>', self._key_release)

    def _focus_out(self, event):
        """Set value to hint if the value is empty."""
        if self._text.get() == "":
            self._text.delete(0, last=END)
            self._text.insert(0, self.hint)
        if self._text.get() == self.hint:
            self._text.configure(foreground=Theme.hint_text_color)
            self.update()

    def _key_press(self, event):
        """If value is hint (nothing written yet), remove the hint."""
        if self._text.get() == self.hint:
            self._text.delete(0, last=END)
            self._text.insert(0, "")
            self._text.icursor(index=0)
            self._text.selection_range(start=0, end=0)

    def _key_release(self, event):
        """Set text color, call callback function and set text.

        If the new value is empty or same as hint, set it to hint and change
        color to Theme.hint_text_color, else set to Theme.text_color and use
        self.show. Call the callback if it exitsts."""
        if self._text.get() == "" or self._text.get() == self.hint:
            self._text.delete(0, last=END)
            self._text.insert(0, self.hint)
            self._text.icursor(index=0)
            self._text.configure(foreground=Theme.hint_text_color, show="")
        else:
            self._text.configure(foreground=Theme.text_color, show=self.show)
        self.update()
        if self.callback is not None:
            self.callback()

    def _mouse(self, event):
        """Set cursor index to 0 and deselect text."""
        if self._text.get() == self.hint:
            self._text.icursor(index=0)
            self._text.selection_range(start=0, end=0)


class SelectPasswordBox(TextBox):

    def _edit_callback(self):
        if len(self.value) > 8:
            self._text.configure(foreground=Theme.good_color)
        elif len(self.value) > 0:
            self._text.configure(foreground=Theme.danger_color)
        else:
            self._text.configure(foreground=Theme.hint_text_color)

    def __init__(self, parent, symbols=None, hint="Password...", card=False,
                 *args, **kwargs):
        self.parent = parent
        super().__init__(parent, hint=hint, symbols=symbols, show="*",
                         callback=self._edit_callback, card=card, boxed=True,
                         *args, **kwargs)


class RetypePasswordBox(TextBox):

    def _edit_callback(self):
        if self.value == self.password:
            self._text.configure(foreground=Theme.good_color)
        elif len(self.value) > 0:
            self._text.configure(foreground=Theme.danger_color)
        else:
            self._text.configure(foreground=Theme.hint_text_color)
        self.update()

    def test(self, password):
        self._edit_callback()
        return True if self.value == self.password else False

    def __init__(self, parent, password, symbols=None,
                 hint="Retype password...", card=False, *args, **kwargs):
        self.parent = parent
        self.password = password
        super().__init__(parent, hint=hint, symbols=symbols, show="*",
                         callback=self._edit_callback, card=card, boxed=True,
                         *args, **kwargs)


class SelectUsernameBox(TextBox):

    def _edit_callback(self):
        pass

    def __init__(self, parent, symbols=None, hint="Username...", show="",
                 *args, **kwargs):
        self.parent = parent
        super().__init__(parent, hint=hint, symbols=symbols, show=show,
                         callback=self._edit_callback, *args, **kwargs)


class DatePicker(Frame):

    """ A date picker widget that allows the user to select a date.

    A user can use the DatePicker widget to select a date (day, month, year).
    It uses three Entry fields to achive this. The day, month and year can be
    changeed with the arrow keys, but there are no buttons to click. The date
    is a standard datetime object. The date can be accessed through the
    property of value. The default date on construction is todays date. The
    widget can be set to allow no date to be the selected date, i.e., date is
    unset. If so, the value property is set to None.

    A callback can be used to get notifyed when the value has been edited.

    USAGE:
        # frame is the tkinter Frame where the widget is to be displayed
        frame = Tk()
        datepicker = DatePicker(parent=frame, allow_unset=True,
                                 label="Date: ")
        # datepicker.callback = foo
        datepicker.value = datetime.datetime.now().date()
        datepicker.pack()  # tkinter displays the widget on frame
        print(str(datepicker.value))  # Prints the time (HH:MM:SS)
        frame.mainloop()
    """

    def __init__(self, parent, allow_unset=False, label="", card=False,
                 boxed=False, callback=None, *args, **kwargs):
        """ Construct the DatePicker widget and give a value of todays date.

        parent - The widget parent where it should be displayed.
        allow_unset - Whether or not the DatePicker can be in a unset state,
                      i.e, not have a date (None).
        label - A string label for the widget. Displayed infrontod the
                datepicker.
        callback - A function that is called every time the text field is
                   edited.
        card - True indicate that the textfield is on a card.
        boxed - If True, create a boxed version.
        *args, **kwargs - Passed to tkinter Frame (see tkinter documentation).
        """
        self.parent = parent
        self.allow_unset = allow_unset
        self.label_text = label
        self.callback = callback
        super().__init__(parent, *args, **kwargs)

        self._value = None
        self.value_day = StringVar()
        self.value_month = StringVar()
        self.value_year = StringVar()
        self.unset = IntVar()
        self.unset.set(0)
        today = datetime.datetime.now().date()
        self.value = today
        self.last_day = today.day
        self.last_month = today.month
        self.last_year = today.year

        # Set configuration values according to input
        border = 0
        space = 0
        background = Theme.window_background
        foreground = Theme.window_background
        if boxed:
            border = 1
            space = 4
            foreground = Theme.card_background
        if card:
            background = Theme.card_background
            foreground = Theme.card_background
        if boxed and card:
            background = Theme.card_background
            foreground = Theme.window_background

        # Create border and space if boxed
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self.columnconfigure(2, weight=1)
        self._border = Frame(self)
        self._border.grid(row=0, column=1, columnspan=1, rowspan=1,
                          sticky="wens")
        self._border.columnconfigure(0, weight=1)
        self._border.rowconfigure(0, weight=1)
        self._space = Frame(self._border)
        self._space.grid(row=0, column=0, columnspan=1, rowspan=1,
                         sticky="wens", padx=(border, border),
                         pady=(border, border))
        self._space.columnconfigure(0, weight=1)
        self._space.rowconfigure(0, weight=1)

        if self.label_text != "":
            self.label = Label(self, text=self.label_text, borderwidth=0)
            self.label.grid(row=0, column=0, columnspan=1, rowspan=1,
                            sticky="wens", padx=(1, space),
                            pady=(space + 3, space))

        self.day = Entry(self._space, textvariable=self.value_day, width=2,
                         highlightthickness=0, borderwidth=0)
        self.day.grid(row=0, column=1, columnspan=1, rowspan=1, sticky="wens",
                      padx=(space, 0), pady=(space + 3, space))

        self.seperator1 = Label(self._space, text="/", borderwidth=0)
        self.seperator1.grid(row=0, column=2, columnspan=1, rowspan=1,
                             sticky="wens", padx=(0, 0),
                             pady=(space + 3, space))

        self.month = Entry(self._space, textvariable=self.value_month, width=2,
                           highlightthickness=0, borderwidth=0)
        self.month.grid(row=0, column=3, columnspan=1, rowspan=1,
                        sticky="wens", padx=(0, 0), pady=(space + 3, space))

        self.seperator2 = Label(self._space, text="/", borderwidth=0)
        self.seperator2.grid(row=0, column=4, columnspan=1, rowspan=1,
                             sticky="wens", padx=(0, 0),
                             pady=(space + 3, space))

        self.year = Entry(self._space, textvariable=self.value_year, width=5,
                          highlightthickness=0, borderwidth=0)
        self.year.grid(row=0, column=5, columnspan=1, rowspan=1, sticky="wens",
                       padx=(0, 1), pady=(space + 3, space))

        self.unset_button = Checkbutton(self._space, text="unset",
                                        command=self._change_unset,
                                        variable=self.unset, onvalue=1,
                                        offvalue=0)
        if self.allow_unset:
            self.unset_button = Checkbutton(self, text="unset",
                                            command=self._change_unset,
                                            variable=self.unset, onvalue=1,
                                            offvalue=0, borderwidth=0)
            self.unset_button.grid(row=0, column=2, columnspan=1, rowspan=1,
                                   sticky="wens", padx=(1, space), pady=(0, 0))

        self.day.bind("<KeyRelease>", self._check)
        self.day.bind('<FocusIn>', self._day_in_focus)
        self.day.bind('<FocusOut>', self._day_out_of_focus)

        self.month.bind("<KeyRelease>", self._check)
        self.month.bind('<FocusIn>', self._month_in_focus)
        self.month.bind('<FocusOut>', self._month_out_of_focus)

        self.year.bind("<KeyRelease>", self._check)
        self.year.bind('<FocusIn>', self._year_in_focus)
        self.year.bind('<FocusOut>', self._year_out_of_focus)

        self.configure(background=background)
        self._border.configure(background=Theme.border_color)
        self._space.configure(background=foreground)
        if self.label_text != "":
            self.label.configure(background=background,
                                 foreground=Theme.text_color,
                                 font=Theme.text_font)
        self.day.configure(background=foreground,
                           foreground=Theme.text_color, font=Theme.text_font,
                           insertbackground=Theme.text_color)
        self.seperator1.configure(background=foreground,
                                  foreground=Theme.text_color,
                                  font=Theme.text_font)
        self.month.configure(background=foreground,
                             foreground=Theme.text_color,
                             font=Theme.text_font,
                             insertbackground=Theme.text_color)
        self.seperator2.configure(background=foreground,
                                  foreground=Theme.text_color,
                                  font=Theme.text_font)
        self.year.configure(background=foreground,
                            foreground=Theme.text_color, font=Theme.text_font,
                            insertbackground=Theme.text_color)
        if self.allow_unset:
            self.unset_button.configure(background=background,
                                        foreground=Theme.text_color,
                                        highlightthickness=0)

    def _day_in_focus(self, event):
        """Bind the up and down keys to the day field."""
        self.day.bind_all("<Up>", self._day_up)
        self.day.bind_all("<Down>", self._day_down)

    def _day_out_of_focus(self, event):
        """Unbind the up and down keys from the day field and fill zeros."""
        self.day.unbind_all("<Up>")
        self.day.unbind_all("<Down>")
        self._check(event)
        self.value_day.set(str(self.value_day.get()).zfill(2))

    def _day_up(self, event):
        """Increases the day value with one (1)."""
        try:
            v = self.value_day.get()
            # Remove all unicode
            temp = v.encode('ascii', 'ignore').decode("utf-8")
            if temp == "":
                temp = self.last_day
            self.value_day.set(int(temp) + 1)
        except Exception as e:
            pass

    def _day_down(self, event):
        """Decreases the day value with one (1)."""
        try:
            v = self.value_day.get()
            # Remove all unicode
            temp = v.encode('ascii', 'ignore').decode("utf-8")
            if temp == "":
                temp = self.last_day
            self.value_day.set(int(temp) - 1)
        except Exception as e:
            pass

    def _month_in_focus(self, event):
        """Bind the up and down keys to the month field."""
        self.month.bind_all("<Up>", self._month_up)
        self.month.bind_all("<Down>", self._month_down)

    def _month_out_of_focus(self, event):
        """Unbind the up and down keys from the month field and fill zeros."""
        self.month.unbind_all("<Up>")
        self.month.unbind_all("<Down>")
        self._check(event)
        self.value_month.set(str(self.value_month.get()).zfill(2))

    def _month_up(self, event):
        """Increases the month value with one (1)."""
        try:
            v = self.value_month.get()
            # Remove all unicode
            temp = v.encode('ascii', 'ignore').decode("utf-8")
            if temp == "":
                temp = self.last_month
            self.value_month.set(int(temp) + 1)
        except Exception as e:
            pass

    def _month_down(self, event):
        """Decreases the month value with one (1)."""
        try:
            v = self.value_month.get()
            # Remove all unicode
            temp = v.encode('ascii', 'ignore').decode("utf-8")
            if temp == "":
                temp = self.last_month
            self.value_month.set(int(temp) - 1)
        except Exception as e:
            pass

    def _year_in_focus(self, event):
        """Bind the up and down keys to the year field."""
        self.year.bind_all("<Up>", self._year_up)
        self.year.bind_all("<Down>", self._year_down)

    def _year_out_of_focus(self, event):
        """Unbind the up and down keys from the year field and fill zeros."""
        self.year.unbind_all("<Up>")
        self.year.unbind_all("<Down>")
        self._check(event)
        self.value_year.set(str(self.value_year.get()).zfill(4))

    def _year_up(self, event):
        """Increases the year value with one (1)."""
        try:
            v = self.value_year.get()
            # Remove all unicode
            temp = v.encode('ascii', 'ignore').decode("utf-8")
            if temp == "":
                temp = self.last_year
            self.value_year.set(int(temp) + 1)
        except Exception as e:
            pass

    def _year_down(self, event):
        """Decreases the year value with one (1)."""
        try:
            v = self.value_year.get()
            # Remove all unicode
            temp = v.encode('ascii', 'ignore').decode("utf-8")
            if temp == "":
                temp = self.last_year
            self.value_year.set(int(temp) - 1)
        except Exception as e:
            pass

    def _check(self, event):
        self._check_year(event)
        self._check_month(event)
        self._check_day(event)
        if self.callback is not None:
            self.callback()

    def _check_day(self, event):
        """Check that the value of day is valid.

        Intended as a callback function for when there is a change in the day
        value. If the new value is invalid, the value is set to self.last_day
        (the last known valid value). 1 <= Day <= [days in selected month].
        """
        _, days = calendar.monthrange(year=int(self.last_year),
                                      month=int(self.last_month))

        if (self.value_day.get() == ""):
            return
        try:
            v = int(self.value_day.get())
            if 1 <= v <= days:
                self.last_day = v
            else:
                if self.last_day > days:
                    self.value_day.set(days)
                else:
                    self.value_day.set(self.last_day)
        except Exception as e:
            self.value_day.set(self.last_day)

    def _check_month(self, event):
        """Check that the value of month is valid.

        Intended as a callback function for when there is a change in the
        month value. If the new value is invalid, the value is set to
        self.last_month (the last known valid value). 1 <= Month <= 12.
        """
        if (self.value_month.get() == ""):
            return
        try:
            v = int(self.value_month.get())
            if 1 <= v <= 12:
                self.last_month = v
            else:
                self.value_month.set(self.last_month)
        except Exception as e:
            self.value_month.set(self.last_month)

    def _check_year(self, event):
        """Check that the value of year is valid.

        Intended as a callback function for when there is a change in the
        year value. If the new value is invalid, the value is set to
        self.last_year (the last known valid value). 0 <= Year <= 9000.
        """
        if (self.value_year.get() == ""):
            return
        try:
            v = int(self.value_year.get())
            if 0 <= v <= 9000:
                self.last_year = v
            else:
                self.value_year.set(self.last_year)
        except Exception as e:
            self.value_year.set(self.last_year)

    def _update_days(self):
        """Update the maximum number of days in selected month and year.

        Intended as a callback function for when the DatePicker changes month
        or year.
        """
        _, days = calendar.monthrange(year=int(self.year.get()),
                                      month=int(self.month.get()))
        self.day.config(to=days)

    def _change_unset(self):
        """Change the DatePickers status according to the unset button.

        Intended as a callback function for when the unset button is toggled.
        If the status is unset, the DatePicker value is set to None, if not the
        value is updated with the date based on the other selevted values.
        """
        if self.unset.get():
            self.value = None
        else:
            date = datetime.datetime(year=int(self.year.get()),
                                     month=int(self.month.get()),
                                     day=int(self.day.get()))
            self.value = date
        if self.callback is not None:
            self.callback()

    @property
    def value(self):
        """Return the selected date as a datetime or None if date is unset."""
        if self.unset.get():
            self._value = None
        else:
            date = datetime.datetime.now()
            date = date.replace(year=int(self.last_year),
                                month=int(self.last_month),
                                day=int(self.last_day))
            self._value = date
        return self._value

    @value.setter
    def value(self, value):
        """Set the selected date to value (datetime). Value can be None if
        unset date is allowed."""
        if value is None and not self.allow_unset:
            return
        self._value = value
        if self._value is None:
            self.unset.set(1)
        else:
            self.unset.set(0)
            self.value_year.set(str(self._value.year).zfill(4))
            self.last_year = self._value.year
            self.value_month.set(str(self._value.month).zfill(2))
            self.last_month = self._value.month
            self.value_day.set(str(self._value.day).zfill(2))
            self.last_day = self._value.day


class TimePicker(Frame):
    """ A TimePicker widget that allows the user to select a time.

    A user can use the TimePicker widget to select a time (hour, minute).
    It uses three Entry fields to achive this. The time is a standard time
    object. The time can be accessed through the property of value. The default
    time on construction is the current time. The widget can be set to allow no
    time to be the selected time, i.e., time is unset. If so, the value
    property is set to None.

    A callback can be used to get notifyed when the value has been edited. The
    callback function can be added to timepicker.callback.

    USAGE:
        # frame is the tkinter Frame where the widget is to be displayed
        frame = Tk()
        timepicker = TimePicker(parent=frame, allow_unset=True,
                                label="Time: ")
        # timepicker.callback = foo
        timepicker.value = datetime.datetime.now().time()
        timepicker.pack()  # tkinter displays the widget on frame
        print(str(timepicker.value))  # Prints the time (HH:MM:SS)
        frame.mainloop()
    """

    def __init__(self, parent, allow_unset=False, label="", callback=None,
                 card=False, boxed=False, *args, **kwargs):
        """ Construct the TimePicker widget and give a value of current time.

        parent - The widget parent where it should be displayed.
        allow_unset - Whether or not the TimePicker can be in a unset state,
                      i.e, not have a time (None).
        label - A string label for the widget. Displayed in front of the
                timepicker.
        callback - A function that is called every time the text field is
                   edited.
        card - True indicate that the textfield is on a card.
        boxed - If True, create a boxed version.
        *args, **kwargs - Passed to tkinter Frame (see tkinter documentation).
        """
        self.parent = parent
        self.allow_unset = allow_unset
        self.label_text = label
        self.callback = callback
        super().__init__(parent, *args, **kwargs)

        self._value = None
        self.value_hour = StringVar()
        self.value_minute = StringVar()
        self.unset = IntVar()
        self.unset.set(0)
        time = datetime.datetime.now().time()
        self.value = time
        self.last_hour = time.hour
        self.last_minute = time.minute

        # Set configuration values according to input
        border = 0
        space = 0
        background = Theme.window_background
        foreground = Theme.window_background
        if boxed:
            border = 1
            space = 4
            foreground = Theme.card_background
        if card:
            background = Theme.card_background
            foreground = Theme.card_background
        if boxed and card:
            background = Theme.card_background
            foreground = Theme.window_background

        # Create border and space if boxed
        self.columnconfigure(1, weight=1)
        self.rowconfigure(1, weight=1)
        self.columnconfigure(2, weight=1)
        self._border = Frame(self)
        self._border.grid(row=0, column=1, columnspan=1, rowspan=1,
                          sticky="wens")
        self._border.columnconfigure(0, weight=1)
        self._border.rowconfigure(0, weight=1)
        self._space = Frame(self._border)
        self._space.grid(row=0, column=0, columnspan=1, rowspan=1,
                         sticky="wens", padx=(border, border),
                         pady=(border, border))
        self._space.columnconfigure(0, weight=1)
        self._space.rowconfigure(1, weight=1)

        if self.label_text != "":
            self.label = Label(self, text=self.label_text, borderwidth=0)
            self.label.grid(row=0, column=0, columnspan=1, rowspan=1,
                            sticky="wens", padx=(1, space),
                            pady=(space + 3, space))
        self.hour = Entry(self._space, textvariable=self.value_hour, width=2,
                          highlightthickness=0, borderwidth=0)
        self.hour.grid(row=0, column=1, columnspan=1, rowspan=1, sticky="wens",
                       padx=(space, 0), pady=(space + 3, space))
        self.seperator = Label(self._space, text=":", borderwidth=0)
        self.seperator.grid(row=0, column=2, columnspan=1, rowspan=1,
                            sticky="wens", padx=(0, 0),
                            pady=(space + 3, space))
        self.minute = Entry(self._space, textvariable=self.value_minute,
                            width=2, highlightthickness=0, borderwidth=0)
        self.minute.grid(row=0, column=3, columnspan=1, rowspan=1,
                         sticky="wens", padx=(0, 4), pady=(space + 3, space))

        if self.allow_unset:
            self.unset_button = Checkbutton(self, text="unset",
                                            command=self._change_unset,
                                            variable=self.unset, onvalue=1,
                                            offvalue=0, borderwidth=0)
            self.unset_button.grid(row=0, column=2, columnspan=1, rowspan=1,
                                   sticky="wens", padx=(1, space), pady=(0, 0))

        self.hour.bind("<KeyRelease>", self._check_hour)
        self.hour.bind('<FocusIn>', self._hour_in_focus)
        self.hour.bind('<FocusOut>', self._hour_out_of_focus)

        self.minute.bind("<KeyRelease>", self._check_minute)
        self.minute.bind('<FocusIn>', self._minute_in_focus)
        self.minute.bind('<FocusOut>', self._minute_out_of_focus)

        self.configure(background=background)
        self._border.configure(background=Theme.border_color)
        self._space.configure(background=foreground)
        if self.label_text != "":
            self.label.configure(background=background,
                                 foreground=Theme.text_color,
                                 font=Theme.text_font)
        self.hour.configure(background=foreground,
                            foreground=Theme.text_color, font=Theme.text_font,
                            insertbackground=Theme.text_color)
        self.seperator.configure(background=foreground,
                                 foreground=Theme.text_color,
                                 font=Theme.text_font)
        self.minute.configure(background=foreground,
                              foreground=Theme.text_color,
                              font=Theme.text_font,
                              insertbackground=Theme.text_color)
        if self.allow_unset:
            self.unset_button.configure(background=background,
                                        foreground=Theme.text_color,
                                        highlightthickness=0)

    def _hour_in_focus(self, event):
        """Bind the up and down keys to the hour field."""
        self.hour.bind_all("<Up>", self._hour_up)
        self.hour.bind_all("<Down>", self._hour_down)

    def _hour_out_of_focus(self, event):
        """Unbind the up and down keys from the hour field and fill zeros."""
        self.hour.unbind_all("<Up>")
        self.hour.unbind_all("<Down>")
        self._check_hour(event)
        self._check_minute(event)
        self.value_hour.set(str(self.value_hour.get()).zfill(2))

    def _minute_in_focus(self, event):
        """Bind the up and down keys to the minute field."""
        self.minute.bind_all("<Up>", self._minute_up)
        self.minute.bind_all("<Down>", self._minute_down)

    def _minute_out_of_focus(self, event):
        """Unbind the up and down keys from the minute field and fill zeros."""
        self.minute.unbind_all("<Up>")
        self.minute.unbind_all("<Down>")
        self._check_hour(event)
        self._check_minute(event)
        self.value_minute.set(str(self.value_minute.get()).zfill(2))

    def _hour_up(self, event):
        """Increases the hour value with one (1)."""
        try:
            v = self.value_hour.get()
            # Remove all unicode
            temp = v.encode('ascii', 'ignore').decode("utf-8")
            if temp == "":
                temp = self.last_hour
            self.value_hour.set(int(temp) + 1)
        except Exception as e:
            pass

    def _hour_down(self, event):
        """Decreases the hour value with one (1)."""
        try:
            v = self.value_hour.get()
            # Remove all unicode
            temp = v.encode('ascii', 'ignore').decode("utf-8")
            if temp == "":
                temp = self.last_hour
            self.value_hour.set(int(temp) - 1)
        except Exception as e:
            pass

    def _minute_up(self, event):
        """Increases the minute value with one (1)."""
        try:
            v = self.value_minute.get()
            # Remove all unicode
            temp = v.encode('ascii', 'ignore').decode("utf-8")
            if temp == "":
                temp = self.last_minute
            self.value_minute.set(int(temp) + 1)
        except Exception as e:
            pass

    def _minute_down(self, event):
        """Decreases the minute value with one (1)."""
        try:
            v = self.value_minute.get()
            # Remove all unicode
            temp = v.encode('ascii', 'ignore').decode("utf-8")
            if temp == "":
                temp = self.last_minute
            self.value_minute.set(int(temp) - 1)
        except Exception as e:
            pass

    def _check_hour(self, event):
        """Check that the value of hour is valid.

        Intended as a callback function for when there is a change in the hour
        value. If the new value is invalid, the value is set to self.last_hour
        (the last known valid value). 0 <= Hour <= 24.
        """
        if (self.value_hour.get() == ""):
            return
        try:
            v = int(self.value_hour.get())
            if 0 <= v <= 23:
                self.last_hour = v
            else:
                self.value_hour.set(self.last_hour)
        except Exception as e:
            self.value_hour.set(self.last_hour)
        if self.callback is not None:
            self.callback()

    def _check_minute(self, event):
        """Check that the value of minute is valid.

        Intended as a callback function for when there is a change in the
        minute value. If the new value is invalid, the value is set to
        self.last_minute (the last known valid value). 0 <= Hour <= 59.
        """
        if (self.value_minute.get() == ""):
            return
        try:
            v = int(self.value_minute.get())
            if 0 <= v <= 59:
                self.last_minute = v
            else:
                self.value_minute.set(self.last_minute)
        except Exception as e:
            self.value_minute.set(self.last_minute)
        if self.callback is not None:
            self.callback()

    def _change_unset(self):
        """Change the TimePickers status according to the unset button.

        Intended as a callback function for when the unset button is toggled.
        If the status is unset, the TimePicker value is set to None, if not the
        value is updated with the date based on the other selected values.
        """
        if self.unset.get():
            self.value = None
        else:
            time = datetime.time(hour=int(self.hour.get()),
                                 minute=int(self.minute.get()), second=int(0))
            self.value = time

    @property
    def value(self):
        """Return the selected time as a time or None if time is unset."""
        if self.unset.get():
            self._value = None
        else:
            time = datetime.time(hour=int(self.last_hour),
                                 minute=int(self.last_minute), second=int(0),
                                 microsecond=int(1))
            self._value = time
        return self._value

    @value.setter
    def value(self, value):
        """Set the selected time to value (time). Value can be None if
        unset date is allowed."""
        if value is None and not self.allow_unset:
            return
        self._value = value
        if self._value is None:
            self.unset.set(1)
        else:
            self.unset.set(0)
            self.value_hour.set(str(self._value.hour).zfill(2))
            self.last_hour = self._value.hour
            self.value_minute.set(str(self._value.minute).zfill(2))
            self.last_minute = self._value.minute


class Selector(Frame):

    """ A Select widget that allows the user to select a value.

    A user can use the Selector widget to select a value. It uses an Entry
    fields to achive this. Selector has a more modern look comapred to
    Combobox. The value can be changeed with the arrow keys, but there are no
    buttons to click. The value is a standard string or object. If objects are
    used, the methodes display_value() and compare_function() must be
    overloaded. The string can be accessed through the property of value. The
    default value on construction is "unknown".

    A callback can be used to get notifyed when the value has been edited.

    A boxed version exist.

    USAGE:
        frame4 = Tk()
        selector = Selector(parent=frame4, options=["work", "private", "home",
                            "mobile", "fax", "reseption", "other"],
                            label="Type: ")
        selector.value = "work"
        selector.pack()  # tkinter displays the widget on frame
        print(str(selector.value))  # Prints "work"
        frame4.mainloop()
    """

    # @property
    # def options(self):
    #     return self._options
    #
    # @options.setter
    # def options(self, options):
    #     self._options = options
    #     try:
    #         self.value = self.options[0]
    #     except Exception as e:
    #         print("No options: ", e)
    #         self.value = None

    def __init__(self, parent, options=None, label="", width=None, boxed=False,
                 card=False, callback=None, *args, **kwargs):
        """ Construct the Selector widget and give a value of "unknown".

        parent - The widget parent where it should be displayed.
        options - List of options (strings) that the user can select from.
        label - A string label for the widget. Displayed infrontod the
                datepicker.
        width - The width of the selector field showing the selected value.
                Default (None) is the width of the widest option.
        callback - A function that is called every time the text field is
                   edited.
        card - True indicate that the textfield is on a card.
        boxed - If True, create a boxed version.
        *args, **kwargs - Passed to tkinter Frame (see tkinter documentation).
        """
        self.parent = parent
        self.options = options
        self.label_text = label
        self.callback = callback
        super().__init__(parent, *args, **kwargs)
        self._value = None
        self._value_selected = StringVar()
        try:
            print(self.options)
            self.value = self.options[0]
        except Exception as e:
            print("RandomException", self.options, e)
            self.value = None
        # Set configuration values according to input
        border = 0
        space = 0
        background = Theme.window_background
        foreground = Theme.window_background
        if boxed:
            border = 1
            space = 4
            foreground = Theme.card_background
        if card:
            background = Theme.card_background
            background = Theme.card_background
        if boxed and card:
            background = Theme.card_background
            foreground = Theme.window_background

        # Create a border and space if boxed
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self._border = Frame(self)
        self._border.grid(row=0, column=1, columnspan=1, rowspan=1,
                          sticky="wens")
        self._border.columnconfigure(0, weight=1)
        self._border.rowconfigure(0, weight=1)
        self._space = Frame(self._border)
        self._space.grid(row=0, column=0, columnspan=1, rowspan=1,
                        sticky="wens", padx=(border, border),
                        pady=(border, border))
        self._space.columnconfigure(0, weight=1)
        self._space.rowconfigure(0, weight=1)
        if self.label_text != "":
            self.label = Label(self, text=self.label_text, borderwidth=0)
            self.label.grid(row=0, column=0, columnspan=1, rowspan=1,
                            sticky="wens", padx=(1, space),
                            pady=(space + 3, space))

        # Calculate the needed width if no width is spesified
        if width is None:
            max_width = 0
            for o in self.options:
                text = self.display_value(value_to_display=o)
                length = len(text)
                max_width = length if length > max_width else max_width
            width = max_width + 1
            # Python tkinter estimate space based on length of string and the
            # size of 0. Since 0 is often larger then other letters, a
            # shrinking is needed.
            # If a currier type of font is used, it should bobably be 1
            # width = int(width * 0.85)
        self._selected = Entry(self._space, textvariable=self._value_selected,
                              highlightthickness=0, borderwidth=0, width=width,
                              justify="center")
        self._selected.grid(row=0, column=1, columnspan=1, rowspan=1,
                           padx=(space, space), pady=(space, space),
                           sticky="wens")
        self._seperator1 = Label(self._space, text="=", borderwidth=0)
        self._seperator1.grid(row=0, column=2, columnspan=1, rowspan=1,
                             sticky="wens", padx=(space % 2, space),
                             pady=(space, space))

        self._selected.bind("<KeyRelease>", self._check)
        self._selected.bind('<FocusIn>', self._selected_in_focus)
        self._selected.bind('<FocusOut>', self._selected_out_of_focus)

        # Configure the GUI
        self.configure(background=background)
        self._border.configure(background=Theme.border_color)
        self._space.configure(background=foreground)
        if self.label_text != "":
            self.label.configure(background=background,
                                 foreground=Theme.text_color,
                                 font=Theme.text_font)
        self._selected.configure(background=foreground,
                                foreground=Theme.text_color,
                                font=Theme.text_font,
                                insertbackground=Theme.text_color)
        self._seperator1.configure(background=foreground,
                                  foreground=Theme.hint_text_color,
                                  font=Theme.text_font)

    def _selected_in_focus(self, event):
        """Bind the up and down keys to the selected field."""
        self._selected.bind_all("<Up>", self._selected_up)
        self._selected.bind_all("<Down>", self._selected_down)

    def _selected_out_of_focus(self, event):
        """Unbind the up and down keys from the selected field."""
        self._selected.unbind_all("<Up>")
        self._selected.unbind_all("<Down>")
        self._check(event)

    def _selected_up(self, event):
        """Increases the selected value with one (1)."""
        try:
            if self._index - 1 >= 0:
                # Up is backwards in the list
                self.value = self.options[self._index - 1]
        except Exception as e:
            print(e)

    def _selected_down(self, event):
        """Decreases the selected value with one (1)."""
        try:
            if self._index + 1 < len(self.options):
                # Down is forward in the list
                self.value = self.options[self._index + 1]
        except Exception as e:
            print(e)

    def _check(self, event):
        try:
            if self.callback is not None:
                self.callback()
        except Exception as e:
            print("Callback error: ", e)

    @property
    def value(self):
        """Return the selected value."""
        return self._value

    @value.setter
    def value(self, value):
        """Set the value as the selected option. Value must be in a possible
        option in the optionlist."""
        if value is None:
            self._value = None
            self._index = None
            self._value_selected.set(" ")
            self._last_value = None
            self._last_index = None
            return
        elif value in self.options:
            self._value = value
            self._index = self.options.index(self._value)
            text = str(self.display_value(value_to_display=self._value))
            self._value_selected.set(text)
            self._last_value = self._value
            self._last_index = self._index
        else:
            for i, o in enumerate(self.options):
                if self.compare_function(value=value, option=o):
                    self._value = self.options[i]  # value
                    self._index = i
                    text = str(self.display_value(
                        value_to_display=self._value))
                    self._value_selected.set(text)
                    self._last_value = self._value
                    self._last_index = self._index

    def display_value(self, value_to_display):
        """Return the string to display in the selector for value_to_display.

        value_to_display is one of the options in the selector. If the options
        are objects, this function must be overloaded. The overloaded function
        must return a string to represent the object. If the options are
        strings, the strings themself will be displayed.

        Example of overload:
            def display_value(self, value_to_display):
                # Where item`s member variable is a string.
                return value_to_display.name"""
        return value_to_display

    def compare_function(self, value, option):
        """Return True if value is equal to option.

        If the options are objects, this function must be overloaded. value is
        the selected value and option is one of the options. The overloaded
        function must return True if the value is equal to the option. If the
        options are strings, the strings are compared.

        Example of overload:
            def compare_function(self, value, option):
                result = True if value.id == option.id else False
                return result"""
        result = True if value == option else False
        return result


class DeleteButton(Label):

    """A delete button widget that can trigger a command function."""

    def __init__(self, parent, command, label="X", *args, **kwargs):
        """Construct the textbox, see the TextBox class for documentation.

        parent - The widget parent where it should be displayed.
        command - The function to be called.
        label - The label, text string to be displayed on the button."""

        super().__init__(parent, text=label, *args, **kwargs)
        self.command = command
        self.configure(background=Theme.window_background,
                       foreground=Theme.hint_text_color,
                       activebackground=Theme.window_background,
                       activeforeground=Theme.danger_color,
                       font=Theme.text_font)
        self._is_over = False
        self.bind("<ButtonRelease-1>", self._relase)
        self.bind("<Enter>", self._on_enter)
        self.bind("<Leave>", self._on_leave)

    def _on_enter(self, event):
        """Set the danger colors."""
        self._is_over = True
        self.configure(background=Theme.window_background,
                       foreground=Theme.danger_color)

    def _on_leave(self, event):
        """Sets the normal colors."""
        self._is_over = False
        self.configure(background=Theme.window_background,
                       foreground=Theme.hint_text_color)

    def _relase(self, event):
        """Call the self.command function."""
        if self._is_over:
            self.command()


class CardHolder(Frame):

    def __init__(self, parent, width=100, *args, **kwargs):
        self.parent = parent
        super().__init__(parent, width=width, *args, **kwargs)
        self.cards = []
        self.rowconfigure(1, weight=1)

        pad = 1
        self.header_value = StringVar()
        self.header_value.set("0%")
        self.footer_value = StringVar()
        self.footer_value.set("100%")

        self.header = Label(self, textvariable=self.header_value, width=4)
        self.header.grid(row=1, column=1, columnspan=1, rowspan=1, sticky="wn",
                         padx=(0, pad), pady=(pad, pad))
        self.footer = Label(self, textvariable=self.footer_value,
                            borderwidth=0, width=4)
        self.footer.grid(row=1, column=1, columnspan=1, rowspan=1, sticky="ws",
                         padx=(0, pad), pady=(pad, pad))
        # Create the canvas
        self.canvas = Canvas(self, bd=0, highlightthickness=0)
        self.canvas.grid(row=1, column=0, columnspan=1, rowspan=1,
                         padx=0, pady=0, sticky="nsw")
        self.canvas.xview_moveto(0)
        self.canvas.yview_moveto(0)
        self.canvas.configure(width=550)

        # Create a frame inside the canvas which will be scrolled with it
        self.interior = Frame(self.canvas)
        self.interior_id = self.canvas.create_window(0, 0,
                                                     window=self.interior,
                                                     anchor="nw")
        # Set events
        self.canvas.bind('<Enter>', self._bound_to_mousewheel)
        self.canvas.bind('<Leave>', self._unbound_to_mousewheel)
        self.interior.bind('<Configure>', self._configure_interior)
        self.canvas.bind('<Configure>', self._configure_canvas)
        # Configure
        self.rowconfigure(1, weight=1)  # The canvas
        self.canvas.rowconfigure(0, weight=1)  # The interior
        self.interior.rowconfigure(0, weight=1)
        self.interior.columnconfigure(0, weight=10)
        self.configure(background=Theme.window_background)
        self.interior.configure(background=Theme.window_background)
        self.canvas.configure(background=Theme.window_background)
        self.header.configure(background=Theme.window_background,
                              foreground=Theme.hint_text_color,
                              font=(Theme.text_font[0], 7))
        self.footer.configure(background=Theme.window_background,
                              foreground=Theme.hint_text_color,
                              font=(Theme.text_font[0], 7))
        self.canvas.yview_moveto(fraction=0)
        self.update()  # Needed because if not yview returns wrong data.
        self._update_labels()
        self.footer_value.set("V")
        self.update()

    def clear_cards(self):
        for c in self.cards:
            c.close()
            c.grid_forget()
            c.destroy()
        self.cards = []

    def add_card(self, card):
        card.create(self.interior)
        card.grid(row=len(self.cards) + 1, column=0, columnspan=1, rowspan=1,
                  sticky="wens", padx=0, pady=0)
        card.show()
        self.update()
        self.cards.append(card)

    def _bound_to_mousewheel(self, event):
        if sys.platform.startswith('linux'):
            self.canvas.bind_all("<Button-4>", self._on_mousewheel)
            self.canvas.bind_all("<Button-5>", self._on_mousewheel)
        else:
            self.canvas.bind_all("<MouseWheel>", self._on_mousewheel)

    def _unbound_to_mousewheel(self, event):
        if sys.platform.startswith('linux'):
            self.canvas.unbind_all("<Button-4>")
            self.canvas.unbind_all("<Button-5>")
        else:
            self.canvas.unbind_all("<MouseWheel>")

    def _on_mousewheel(self, event):
        if sys.platform.startswith('linux'):
            if event.num == 4:  # scrolling up
                c = self.canvas.yview()
                if c[0] != 0:  # Avoid overscrolling
                    self.canvas.yview('scroll', -1, 'units')
                    # TODO - Refresh content?
            elif event.num == 5:  # scrolling down
                self.canvas.yview('scroll', 1, 'units')
        else:
            self.canvas.yview_scroll(int(-1.0 * (event.delta)), "units")
        self._update_labels()

    def _update_labels(self):
        # Update scrollbar labels according to possition
        c = self.canvas.yview()
        y = int(c[0] * 100)
        y = "{}%".format(y)
        self.header_value.set(y)
        y = int(c[1] * 100)
        y = "{}%".format(y)
        self.footer_value.set(y)

    def _configure_interior(self, event):
        # Update the scrollbars to match the size of the inner frame
        size = (self.interior.winfo_reqwidth(),
                self.interior.winfo_reqheight())
        self.canvas.config(scrollregion="0 0 %s %s" % size)
        # The code below caused scaling issues for the canvas.
        # if self.interior.winfo_reqwidth() != self.canvas.winfo_width():
        #     # Update the canvas's width to fit the inner frame
        #     self.canvas.config(width=self.interior.winfo_reqwidth())

    def _configure_canvas(self, event):
        if self.interior.winfo_reqwidth() != self.canvas.winfo_width():
            # Update the inner frame's width to fill the canvas
            self.canvas.itemconfigure(self.interior_id,
                                      width=self.canvas.winfo_width())
            # self.canvas.height = self.canvas.winfo_reqheight()


class Card(Frame):
    """A Card class for the CardHolder class. Intened as a base class.

    This class shows the required interface for the card class."""

    def __init__(self):
        """Constructor can be overlaoded. May take in data for later use."""
        pass

    def create(self, parent, *args, **kwargs):
        """Create the frame in parent. Called by the CardHolder."""
        self.parent = parent
        super().__init__(parent, *args, **kwargs)

    def show(self):
        """Create the gui with data. Called by the CardHolder. Intended to be
        overloaded. Remember to set up binding for events such as clicking on
        the card."""
        pass

    def close(self):
        """Called before card is closed by the CardHolder. Intended to be
        overloaded."""
        pass

    def refresh(self):
        """Refresh the data and or gui in the card. Intended to be
        overloaded. Called by the CardHolder when it recives a callback."""
        pass

    def selected(self):
        """Recomanded, not required."""
        pass

    def highligthed(self):
        """Recomanded, not required."""
        pass


class _Dummy():
    """Dummy class for testing"""

    def __init__(self, dummy_id, name, age):
        self.id = dummy_id
        self.name = name
        self.age = age


class DummySelector(Selector):

    def __init__(self, parent, options=None, label="", width=8, *args,
                 **kwargs):
        super().__init__(parent, options=options, label="", width=8, *args,
                         **kwargs)

    def display_value(self, value_to_display):
        """Return the value to display. Intended to be overloaded."""
        return value_to_display.name

    def compare_function(self, value, option):
        """Return True if value is equal to option. Intended to be
        overloaded."""
        result = True if value == option.id else False
        print("Test: ", value, option.id, result)
        return result


def command_delete():
    print("delete")


class tTk(Tk):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title("Journal")
        self.configure(background=Theme.window_background)
        self.geometry("750x750")


if __name__ == "__main__":
    """Create a tkinter gui window with a DatePicker."""
    Theme.set_dark_theme()
    Theme.list_fonts()
    # frame is the tkinter Frame where the widget is to be displayed

    frame = tTk()
    text_field = TextBox(parent=frame, hint="First name...")
    text_field.pack(padx=4, pady=4)  # tkinter displays the widget on frame
    frame.mainloop()

    frame = tTk()
    text_box = TextBox(parent=frame, hint="Last name...")
    text_box.pack(padx=4, pady=4)  # tkinter displays the widget on frame
    frame.mainloop()

    frame = tTk()
    datepicker = DatePicker(parent=frame, allow_unset=True,
                            label="Birthday: ")
    datepicker.value = datetime.datetime(year=1970, month=1, day=1).date()
    datepicker.pack(padx=4, pady=4)  # tkinter displays the widget on frame
    print(str(datepicker.value))  # Prints the date (01.01.1970)
    frame.mainloop()

    frame = tTk()
    timepicker = TimePicker(parent=frame, allow_unset=True, label="Time: ")
    timepicker.value = datetime.datetime.now().time()
    timepicker.pack(padx=4, pady=4)  # tkinter displays the widget on frame
    print(str(timepicker.value))  # Prints the time (HH:MM:SS)
    frame.mainloop()

    frame = tTk()
    selector = Selector(parent=frame, options=["work", "private", "home",
                        "mobile", "fax", "reseption", "other"], label="Type: ")
    selector.value = "work"
    selector.pack(padx=4, pady=4)  # tkinter displays the widget on frame
    print(str(selector.value))  # Prints "work"
    frame.mainloop()

    d1 = _Dummy(dummy_id=1, name="Adam", age=19)
    d2 = _Dummy(dummy_id=2, name="Eve", age=19)
    d3 = _Dummy(dummy_id=3, name="John", age=21)
    frame = tTk()
    selector = DummySelector(parent=frame, options=[d1, d2, d3],
                             label="Type: ")
    selector.value = 2
    selector.pack(padx=4, pady=4)  # tkinter displays the widget on frame
    print(str(selector.value.name))  # Prints "Eve"
    frame.mainloop()

    frame = tTk()
    selector = Selector(parent=frame, options=["work", "private", "home",
                        "mobile", "fax", "reseption", "other",
                        "A very, very long string for testing"],
                        label="Type: ", boxed=True)
    selector.value = "work"
    selector.pack(padx=4, pady=4)  # tkinter displays the widget on frame
    print(str(selector.value))  # Prints "work"
    frame.mainloop()

    frame = tTk()
    deletebutton = DeleteButton(parent=frame, command=command_delete,
                                label="X")
    deletebutton.pack(padx=4, pady=4)  # tkinter displays the widget on frame
    frame.mainloop()
