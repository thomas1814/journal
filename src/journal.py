#!/usr/bin/env python3
from resources import logger
from tkinter import Label, Frame
from widgets import TextField, TextBox, Selector, DeleteButton, Theme, Card
from database import Database
from entrywindow import EntryWindow

""" The journal module. """

__author__ = "Thomas Rostrup Andersen"
__copyright__ = """Copyright (C) 2017 Thomas Rostrup Andersen.
        All rights reserved."""
__license__ = "BSD 2-clause"
__version__ = "0.1.8"
__all__ = ["CardEntry", "JournalSelector"]

logger.debug("Journal module imported.")


class JournalSelector(Selector):

    def __init__(self, parent, options=None, label="", width=None, boxed=True,
                 callback=None, *args, **kwargs):
        super().__init__(parent, options=options, label=label, width=width,
                         card=False, boxed=False, callback=callback, *args,
                         **kwargs)
        self._selected.configure(foreground=Theme.headline_color,
                                 font=Theme.headline_font,
                                 insertbackground=Theme.text_color)

    def display_value(self, value_to_display):
        return value_to_display.name

    def compare_function(self, value, option):
        result = True if value == option.journal_id else False
        return result


class CardEntry(Card):

    def __init__(self, entry_id=None, callback=None, *args, **kwargs):
        self.entry_id = entry_id
        self.callback = callback

    def show(self):
        self.configure(width=100)
        border = 0
        self.columnconfigure(0, weight=4)
        self.border = Frame(self)
        self.border.configure(background=Theme.hint_text_color)
        self.border.grid(row=0, column=0, columnspan=2, rowspan=1,
                         padx=(3, 3), pady=(3, 3), sticky="wens")
        self.border.columnconfigure(0, weight=1)
        self.border.rowconfigure(0, weight=1)
        self.card = Frame(self.border, width=100)
        self.card.grid(row=0, column=0, columnspan=1, rowspan=1, sticky="wns",
                       padx=border, pady=(1, 0))
        self.card.columnconfigure(0, weight=1)
        try:
            self.title = TextBox(self.card, card=False, editable=False,
                                 font="title")
            self.title.grid(row=0, column=0, columnspan=1, rowspan=1,
                            sticky="wens", padx=10, pady=(5, 0))
            self.text = TextField(self.card, card=False, height=3,
                                  editable=False, scrollbar=False)
            self.text.grid(row=1, column=0, columnspan=2, rowspan=1,
                           sticky="wens", padx=10, pady=0)
            self.location = Label(self.card)
            self.location.grid(row=2, column=0, columnspan=1, rowspan=1,
                               sticky="w", padx=10, pady=(0, 3))
            self.author = Label(self.card)
            self.delete_button = DeleteButton(parent=self.card,
                                              command=self.delete_card)
            self.delete_button.grid(row=0, column=1, columnspan=1, rowspan=1,
                                    sticky="ens", padx=(5, 5), pady=(5, 5))
        except Exception as e:
            logger.exception("Unable to load entry card...")
            print("show", e)
        else:
            self.bind("<Double-Button-1>", self.selected)
            self.border.bind("<Double-Button-1>", self.selected)
            self.card.bind("<Double-Button-1>", self.selected)
            self.card.bind("<Double-Button-1>", self.selected)
            self.title.bind("<Double-Button-1>", self.selected)
            self.text.bind("<Double-Button-1>", self.selected)
            self.location.bind("<Double-Button-1>", self.selected)
            self._set_theme()
        self.refresh()

    def refresh(self):
        record = Database.get_entry(entry_id=self.entry_id)[0]
        self.title.value = "{}".format(record.entry_title)
        value = record.entry_text
        index = value.find("\n")
        index = index if index < 200 and not -1 else 200
        self.text.value = record.entry_text[0:index] + "..."
        day = record.entry_date.date().day
        month = record.entry_date.date().month
        year = record.entry_date.date().year
        hour = record.entry_date.time().hour
        minute = record.entry_date.time().minute
        footer = "{}/{}/{} - {}:{} - {}".format(day, month, year, hour, minute,
                  record.location)
        self.location.config(text=footer)
        record = Database.get_user(user_id=record.user_id)[0]
        self.author.config(text="@{}".format(record.first_name))

    def selected(self, event):
        logger.debug("This card was selected: {}...".format(self.entry_id))
        self.entry = EntryWindow(self.parent)
        self.entry.callback_close = self.refresh
        self.entry.load(entry_id=self.entry_id)

    def delete_card(self):
        logger.debug("This card was deleted: {}...".format(self.entry_id))
        Database.delete_entry(entry_id=self.entry_id)
        self.callback()

    def _set_theme(self):
        self.configure(background=Theme.window_background)
        self.card.configure(background=Theme.window_background)
        self.location.configure(background=Theme.window_background,
                                foreground=Theme.subscript_color,
                                font=Theme.subscript_font)
        self.author.configure(background=Theme.window_background,
                              foreground=Theme.subscript_color,
                              font=Theme.subscript_font)
