#!/usr/bin/env python3
import os
from tkinter import Text, WORD, DISABLED, END, Frame
from resources import logger
from applicationdatabase import AppDB
from widgets import TextBox, Theme, tButton

""" The path frame module. """

__author__ = "Thomas Rostrup Andersen"
__copyright__ = """Copyright (C) 2017 Thomas Rostrup Andersen.
        All rights reserved."""
__license__ = "BSD 2-clause"
__version__ = "0.1.8"
__all__ = ["PathFrame"]

logger.debug("The path frame imported.")


class PathFrame(Frame):

    """A path selection frame. Allows the user to select a path."""

    def __init__(self, parent, callback_function=None, *args, **kwargs):
        self.parent = parent
        self.callback_function = callback_function
        super().__init__(parent, *args, **kwargs)

        message = "No userdata found. Select a location to store new userdata \
                   or a path that contains userdata:"
        self.label = Text(self, width=50, wrap=WORD, height=2)
        self.label.delete(0.0, END)
        self.label.insert(END, message)
        self.label.grid(row=0, column=1, columnspan=1, rowspan=1,
                        sticky="wens", padx=(20, 20), pady=(20, 20))
        self.label.configure(state=DISABLED, yscrollcommand=None,
                             background=Theme.window_background,
                             foreground=Theme.text_color, borderwidth=0,
                             highlightthickness=0, font=Theme.text_font)
        self.configure(background=Theme.window_background)

        self.path = TextBox(self, hint="Path...", boxed=True)
        self.path.grid(row=1, column=1, columnspan=1, rowspan=1, sticky="wen",
                       padx=(20, 20), pady=(5, 20))
        home = os.path.expanduser("~/journal/")
        home = os.path.normcase(home)
        self.path.value = home
        self._continue_button = tButton(self, label="Continue...",
                                        callback=self._click_continue)
        self._continue_button.grid(row=2, column=1, columnspan=1, rowspan=1,
                                   sticky="se", padx=(20, 20), pady=(0, 10))

    def _click_continue(self):
        if not os.path.isdir(self.path.value):
            os.makedirs(self.path.value)
        if os.path.isdir(self.path.value):
            AppDB.add_record(key="user_path", value=self.path.value)
            if self.callback_function is not None:
                self.callback_function()
        else:
            self.label.insert(END, "Not a valid path.")
