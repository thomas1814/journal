#!/usr/bin/env python3
from tkinter import END, Frame, Listbox, Scrollbar
from resources import logger
from database import Database
from widgets import TextBox, DatePicker, Selector, Theme, tButton, TextField
import datetime

""" The book shelf frame module. """

__author__ = "Thomas Rostrup Andersen"
__copyright__ = """Copyright (C) 2017 Thomas Rostrup Andersen.
        All rights reserved."""
__license__ = "BSD 2-clause"
__version__ = "0.1.8"
__all__ = ["BookShelfFrame"]

logger.debug("The book shelf frame imported.")


class BookShelfFrame(Frame):
    """Create a new journal book shelf frame."""

    def __init__(self, parent, *args, **kwargs):
        """Create a new frame with widgets.

        * parent: Tk parent frame."""
        # Frame configuration
        self.parent = parent
        super().__init__(parent, *args, **kwargs)
        self.configure(background=Theme.window_background)
        self.columnconfigure(0, weight=0)
        self.columnconfigure(1, weight=1)
        self.description_frame = None

        # Create the search frame
        self.search_frame = Frame(self)
        self.search_frame.configure(background=Theme.window_background)
        self.search_frame.grid(row=0, column=0, columnspan=1, rowspan=1,
                               sticky="wens", padx=0, pady=0)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self.search = TextBox(self.search_frame, hint="Search...",
                              justify="center", boxed=True, card=False)
        self.search.grid(row=1, column=1, columnspan=1, rowspan=1,
                         padx=(20, 0), pady=(0, 0))
        self.search_frame.columnconfigure(1, weight=1)
        self.new_button = tButton(self.search_frame, label="+",
                                  callback=self._new)
        self.new_button.grid(row=1, column=2, columnspan=1, rowspan=1,
                             sticky="wens", padx=(0, 0), pady=(0, 0))
        self.scrollbar = Scrollbar(self.search_frame)
        self._list = Listbox(self.search_frame,
                             yscrollcommand=self.scrollbar.set, height=20)
        self._list.configure(background=Theme.window_background, borderwidth=0,
                             foreground=Theme.text_color, font=Theme.text_font,
                             highlightthickness=0)
        self._list.bind("<Double-Button-1>", self._change_journal)
        self._list.bind("<Return>", self._change_journal)
        self._list.bind("<Delete>", self._delete)
        self.search_frame.rowconfigure(2, weight=1)
        self._list.grid(row=2, column=1, columnspan=2, rowspan=1,
                        sticky="wens", padx=20, pady=10)
        self._load_list()

    def _build_description_frame(self):
        """Build and show the journal description frame."""
        self.description_frame = Frame(self)
        self.description_frame.configure(background=Theme.window_background)
        self.description_frame.grid(row=0, column=1, columnspan=1, rowspan=1,
                                    sticky="wens", padx=0, pady=0)
        self.rowconfigure(0, weight=1)
        self.description_frame.columnconfigure(1, weight=1)
        self.description_frame.columnconfigure(2, weight=2)
        self.name = TextBox(self.description_frame, hint="Journal Name...",
                            callback=self._save)
        self.name.grid(row=0, column=0, columnspan=1, rowspan=1, sticky="wen",
                       padx=(30, 20), pady=(30, 0))
        self.date_from = DatePicker(self.description_frame,
                                    label="Start Date: ", callback=self._save)
        self.date_from.grid(row=1, column=0, columnspan=1, rowspan=1,
                            sticky="wn", padx=(30, 20), pady=0)
        self.date_to = DatePicker(self.description_frame, allow_unset=True,
                                  label="End Date: ", callback=self._save)
        self.date_to.grid(row=2, column=0, columnspan=1, rowspan=1,
                          sticky="wn", padx=(30, 20), pady=0)
        self.type = Selector(self.description_frame, options=["work",
                             "private", "life"], label="Type: ",
                             callback=self._save)
        self.type.value = "work"
        self.type.grid(row=3, column=0, columnspan=1, rowspan=1, sticky="wn",
                       padx=(30, 20), pady=0)
        self.status = Selector(self.description_frame, options=["active",
                               "archived"], label="Status: ",
                               callback=self._save)
        self.status.grid(row=4, column=0, columnspan=1, rowspan=1, sticky="wn",
                         padx=(30, 20), pady=0)
        self.text = TextField(self.description_frame,
                              hint="Describe the journal...",
                              callback=self._save, width=40, height=5)
        self.text.grid(row=6, column=0, columnspan=2, rowspan=1, padx=(30, 0),
                       pady=(5, 10))

    def _activate_autosave(self):
        """Save when text is edited."""
        self.name.bind("<KeyRelease>", self._save)
        self.text.bind("<KeyRelease>", self._save)

    def _change_journal(self, event):
        """Change the journal discription to an other journal."""
        if self.description_frame is not None:
            self.description_frame.destroy()
            self.description_frame = None
        try:
            self._build_description_frame()
            self.index = int(self._list.curselection()[0])
            self.journal_id = self.values[self.index].journal_id
            self._load_description(journal_id=self.journal_id)
            self._list.activate(self.index)
        except Exception as e:
            logger.exception(e)
        else:
            self._activate_autosave()

    def _load_list(self):
        """Load the list of journals into the form"""
        self._list.delete(0, END)
        if self.search.value == "":
            self.values = Database.get_all_journals()
        else:
            pass
            # TODO - Add search terms for searching among journals
        for i, v in enumerate(self.values):
            self._list.insert(i, (str(v.name)))

    def _load_description(self, journal_id):
        """Load the journal description with journal_id into the form."""
        self.record = Database.get_journal(journal_id=self.journal_id)[0]
        self.name.value = self.record.name
        self.date_to.value = self.record.date_to.date() if \
            self.record.date_to is not None else None
        self.date_from.value = self.record.date_from.date() if \
            self.record.date_from is not None else None
        self.type.value = self.record.record_type
        self.status.value = self.record.status
        self.text.value = self.record.description

    def _new(self):
        """Create a new journal in the database and load it on the form."""
        if self.description_frame is not None:
            self.description_frame.destroy()
        self._build_description_frame()
        self.update()
        self.journal_id = Database.add_journal(name="Journal Name...",
                                               record_type="life",
                                               status="active",
                                               description="",
                            date_from=datetime.datetime.now(), date_to=None)
        self._load_description(journal_id=self.journal_id)
        self._load_list()
        logger.debug("New journal with journal_id {} created.".format(
                     self.journal_id))
        self._activate_autosave()

    def _save(self, event=None):
        """Save the current values on the form."""
        description = self.text.value
        date_from = self.date_from.value
        date_to = self.date_to.value
        name = "Journal Name..." if self.name.value == "" else self.name.value
        Database.update_journal(journal_id=self.journal_id,
                                name=name,
                                record_type=self.type.value,
                                status=self.status.value,
                                description=description, date_from=date_from,
                                date_to=date_to)
        self._load_list()

    def _delete(self, event=None):
        """Delete the selected journal from the database and update the list on
        the form."""
        self.index = int(self._list.curselection()[0])
        journal_id = self.values[self.index].journal_id
        status = Database.delete_journal(journal_id=journal_id)
        if status:
            if self.index > self._list.size():
                self.index -= 1
        logger.debug("Deleted journal {}: {}.".format(journal_id, status))
        self._list.activate(self.index)
        self._load_list()
        if self.description_frame is not None:
            self.description_frame.destroy()
        self.update()
