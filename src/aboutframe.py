#!/usr/bin/env python3
from tkinter import DISABLED, Frame, Label
from resources import logger
from widgets import Theme

""" The about frame module. """

__author__ = "Thomas Rostrup Andersen"
__copyright__ = """Copyright (C) 2017 Thomas Rostrup Andersen.
        All rights reserved."""
__license__ = "BSD 2-clause"
__version__ = "0.1.8"
__all__ = ["AboutFrame"]

logger.debug("The about frame imported.")


class AboutFrame(Frame):

    """An about frame."""

    def __init__(self, parent, callback_function=None, *args, **kwargs):
        self.parent = parent
        self.callback_function = callback_function
        super().__init__(parent, *args, **kwargs)

        message = """
        About:

        Journal 0.1.8
        Copyright (c) 2017, Thomas Rostrup Andersen.
        All rights reserved.
        License: BSD 2-clause

        Icons by www.icons8.com
        license: CC BY-ND 3.0
        """

        self.label = Label(self, text=message)
        self.label.grid(row=0, column=1, columnspan=1, rowspan=1,
                        sticky="wens", padx=(20, 20), pady=(20, 20))
        self.label.configure(state=DISABLED, yscrollcommand=None,
                             background=Theme.window_background,
                             foreground=Theme.text_color, borderwidth=0,
                             highlightthickness=0, font=Theme.text_font)
        self.configure(background=Theme.window_background)
        self.columnconfigure(1, weight=1)
