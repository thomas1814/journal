#!/usr/bin/env python3
from contextlib import suppress
from PIL import Image, ImageTk, _tkinter_finder  # Required by PyInstaller
from tkinter import Tk, Frame, PhotoImage
from resources import logger, get_resource_path
from journal import JournalSelector, CardEntry, EntryWindow
from database import Database
from applicationdatabase import AppDB
from widgets import TextBox, Theme, tButton, tLink, CardHolder
from licenseframe import LicenseFrame
from signupframe import SignUpFrame
from aboutframe import AboutFrame
from pathframe import PathFrame
from bookshelfframe import BookShelfFrame

""" The apllication module. """

__author__ = "Thomas Rostrup Andersen"
__copyright__ = """Copyright (C) 2017 Thomas Rostrup Andersen.
        All rights reserved."""
__license__ = "BSD 2-clause"
__version__ = "0.1.8"
__all__ = ["MainFrame", "App"]

logger.debug("Application started.")


class MainFrame(Frame):

    def open_book_shelf(self):
        self._destroy_body_frame()
        self._build_book_frame()
        self._book_shelf.selected = True

    def open_about(self):
        self._destroy_body_frame()
        self._build_about_frame()
        self._about.selected = True

    def open_journal(self):
        self._destroy_body_frame()
        journals = Database.get_all_journals()
        if len(journals) > 0:
            self._build_journal_frame()
            self._journal_button.selected = True
            selected_id = Database.get_setting(key="selected_journal")[0].value
            self._selected_journal.value = selected_id
            self._change_journal()
        else:
            self.open_book_shelf()
        self.update()

    def __init__(self, parent, *args, **kwargs):
        self._parent = parent
        super().__init__(parent, *args, **kwargs)
        self.configure(background=Theme.window_background)
        self.rowconfigure(0, weight=1)
        self.columnconfigure(2, weight=1)

        self._user_id = 1
        self._selected_journal = None
        self._journal_frame = None
        self._book_frame = None
        self._about_frame = None

        # App margin frame
        self._margin = Frame(self, width=80, background=Theme.card_background)
        self._margin.grid(row=0, column=0, columnspan=1, rowspan=3,
                          sticky="wens", padx=(0, 0), pady=(0, 0))

        # Create the journal button
        self.filename = get_resource_path(name="/icons8/journal_white.png")
        self.orginal = Image.open(self.filename)
        self.image_tk = ImageTk.PhotoImage(self.orginal)
        self._journal_button = tLink(self._margin, image=self.image_tk,
                                     label="", callback=self.open_journal)
        self._journal_button.grid(row=3, column=6, columnspan=1, rowspan=1,
                                  sticky="wns", padx=(0, 0), pady=(40, 0))
        self.filename = get_resource_path(name="/icons8/journal_black.png")
        self.orginal = Image.open(self.filename)
        self.image_tk = ImageTk.PhotoImage(self.orginal)
        self._journal_button.image_pressed = self.image_tk
        self._journal_button.image_selected = self.image_tk
        self.filename = get_resource_path(name="/icons8/journal_orange.png")
        self.orginal = Image.open(self.filename)
        self.image_tk = ImageTk.PhotoImage(self.orginal)
        self._journal_button.image_hovering = self.image_tk
        self._journal_button.image_focused = self.image_tk

        # Create the book shelf button
        self.filename = get_resource_path(name="/icons8/bookshelf_white.png")
        self.orginal = Image.open(self.filename)
        self.image_tk = ImageTk.PhotoImage(self.orginal)
        self._book_shelf = tLink(self._margin, image=self.image_tk, label="",
                                 callback=self.open_book_shelf)
        self._book_shelf.grid(row=4, column=6, columnspan=1, rowspan=1,
                              sticky="wns", padx=(0, 0), pady=(0, 0))
        self.filename = get_resource_path(name="/icons8/bookshelf_black.png")
        self.orginal = Image.open(self.filename)
        self.image_tk = ImageTk.PhotoImage(self.orginal)
        self._book_shelf.image_pressed = self.image_tk
        self._book_shelf.image_selected = self.image_tk
        self.filename = get_resource_path(name="/icons8/bookshelf_orange.png")
        self.orginal = Image.open(self.filename)
        self.image_tk = ImageTk.PhotoImage(self.orginal)
        self._book_shelf.image_hovering = self.image_tk
        self._book_shelf.image_focused = self.image_tk

        # Create the about button
        self.filename = get_resource_path(name="/icons8/about_white.png")
        self.orginal = Image.open(self.filename)
        self.image_tk = ImageTk.PhotoImage(self.orginal)
        self._about = tLink(self._margin, image=self.image_tk, label="",
                            callback=self.open_about)
        self._about.grid(row=5, column=6, columnspan=1, rowspan=1,
                         sticky="wns", padx=(0, 0), pady=(0, 0))
        self.filename = get_resource_path(name="/icons8/about_black.png")
        self.orginal = Image.open(self.filename)
        self.image_tk = ImageTk.PhotoImage(self.orginal)
        self._about.image_pressed = self.image_tk
        self._about.image_selected = self.image_tk
        self.filename = get_resource_path(name="/icons8/about_orange.png")
        self.orginal = Image.open(self.filename)
        self.image_tk = ImageTk.PhotoImage(self.orginal)
        self._about.image_hovering = self.image_tk
        self._about.image_focused = self.image_tk

        # Journal body frame
        self._body = Frame(self, height=100, width=100,
                           background=Theme.window_background)
        self._body.grid(row=0, column=2, columnspan=1, rowspan=3,
                        sticky="wens", padx=10, pady=10)
        self._body.rowconfigure(0, weight=40)
        self._body.columnconfigure(1, weight=0)
        self.open_journal()

    def _build_journal_frame(self):
        self._journal_frame = Frame(self._body, height=100, width=100,
                                    background=Theme.window_background)
        self._journal_frame.grid(row=0, column=1, columnspan=1, rowspan=1,
                                 sticky="wens", padx=10, pady=10)
        self._header = Frame(self._journal_frame, height=20, width=650,
                             background=Theme.window_background)
        self._header.grid(row=0, column=1, columnspan=2, rowspan=1,
                          sticky="wens", padx=10, pady=10)
        self._header.columnconfigure(8, weight=1)
        self._header.columnconfigure(4, weight=1)
        self._new_button = tButton(self._header, label="+",
                                   callback=self._new_entry)
        self._new_button.grid(row=0, column=8, columnspan=1, rowspan=1,
                              sticky="e", padx=(10, 10), pady=(10, 10))
        self.search = TextBox(self._header, hint="Search...", justify="center",
                              callback=self._search, card=False, boxed=True)
        self.search.grid(row=0, column=4, columnspan=1, rowspan=1,
                         sticky="wen", padx=(40, 10), pady=(10, 10))
        journals = Database.get_all_journals()
        self._selected_journal = JournalSelector(parent=self._header,
                                                 options=journals, label="",
                                                 callback=self._change_journal)
        self._selected_journal.grid(row=0, column=0, columnspan=1, rowspan=1,
                                    sticky="wens", padx=(10, 10),
                                    pady=(10, 10))
        self.update()

    def _build_book_frame(self):
        self._book_frame = BookShelfFrame(parent=self._body)
        self._book_frame.grid(row=0, column=1, columnspan=1, rowspan=1,
                              sticky="wens", padx=10, pady=10)
        self.update()

    def _build_about_frame(self):
        self._body.columnconfigure(1, weight=1)
        self._about_frame = AboutFrame(parent=self._body)
        self._about_frame.grid(row=0, column=1, columnspan=1, rowspan=1,
                               sticky="wens", padx=10, pady=(200, 100))
        self.update()

    def _destroy_body_frame(self):
        if self._journal_frame is not None:
            self._journal_frame.destroy()
            self._journal_frame = None
            self._journal_button.selected = False
        if self._book_frame is not None:
            self._book_frame.destroy()
            self._book_frame = None
            self._book_shelf.selected = False
        self._journal_button.selected = False
        self._book_shelf.selected = False
        self._about.selected = False
        self._body.columnconfigure(1, weight=0)

    def _new_entry(self):
        logger.debug("Create a new entry.")
        new_entry = EntryWindow(parent=self._parent,
                                callback_close=self._change_journal)
        new_entry.new(journal_id=self._selected_journal.value.journal_id,
                      user_id=self._user_id)
        self._change_journal()

    def _change_journal(self):
        try:
            self._journal_frame.columnconfigure(1, weight=40)
            self._journal_frame.rowconfigure(2, weight=40)
            self._card_holder = CardHolder(parent=self._journal_frame,
                                           width=20)
            entries = Database.get_entries_to(
                journal_id=self._selected_journal.value.journal_id)
            for e in entries:
                card = CardEntry(callback=self._change_journal,
                                 entry_id=e.entry_id)
                self._card_holder.add_card(card=card)
            self._card_holder.canvas.update_idletasks()
            self._card_holder.grid(row=2, column=1, columnspan=1, rowspan=1,
                                   sticky="wns", padx=(10, 20), pady=0)
        except Exception as e:
            print("Change: ", e)

    def _search(self):
        print("searching...")


class App(Tk):

    def __init__(self, *args, **kwargs):
        try:
            super().__init__(*args, **kwargs)
        except Exception as e:
            logger.exception(e)
        self.pages = []
        Theme.set_dark_theme()
        # Theme.set_ligth_theme()
        # Theme.list_fonts()
        # Theme.font_viewer()
        self.title("Journal")
        self.configure(background=Theme.window_background)
        self.geometry("750x750")
        logger.debug("getting resource")
        filename = get_resource_path(name='/icons8/icon_large.png')
        icon = PhotoImage(file=filename)
        logger.debug("icon stuff")
        self.tk.call('wm', 'iconphoto', self._w, icon)
        self.deiconify()  # .iconify() .state('normal') state = .state()
        logger.debug("running controller")
        self._controller()

    def _controller(self):
        # Destroy old frames
        with suppress(Exception):
            self.path_frame.destroy()
        with suppress(Exception):
            self.sign_up_frame.destroy()
        with suppress(Exception):
            self.main_frame.destroy()
        with suppress(Exception):
            self.license_frame.destroy()
        try:
            licenses = AppDB.get_license_not_accepted()
            license_ids = [license.license_id for license in licenses]
            if license_ids:
                self.license_frame = LicenseFrame(license_id=license_ids[0],
                        parent=self, callback_function=self._controller)
                self.license_frame.place(anchor="c", relx=.5, rely=.4)
                self.license_frame.tkraise()
                return
            # Check if a path to userdata exist, if not ask for a path
            path = AppDB.get_record("user_path")
            if not path:
                self.path_frame = PathFrame(self,
                                        callback_function=self._controller)
                self.path_frame.place(anchor="c", relx=.5, rely=.4)
                self.path_frame.tkraise()
            else:
                # Check is a user exist, if not ask user to sign up
                Database.create(filename=path[0].value + "journal.db")
                user = Database.get_all_users()
                if not user:
                    self.sign_up_frame = SignUpFrame(self,
                                        callback_function=self._controller)
                    self.sign_up_frame.place(anchor="c", relx=.5, rely=.4)
                    self.sign_up_frame.tkraise()
                else:
                    # Run main frame
                    self.main_frame = MainFrame(self)
                    self.main_frame.place(x=0, y=0, relwidth=1, relheight=1)
                    self.main_frame.tkraise()
        except Exception as e:
            logger.exception("Controller: ", e)

    def run(self):
        self.lift()
        self.attributes("-topmost", True)
        self.after_idle(self.attributes, '-topmost', False)
        # dirname = filedialog.askdirectory()
        self.mainloop()


if __name__ == "__main__":
    logger.debug("Main started.")
    AppDB.create()
    AppDB.refresh()
    app = App()
    logger.debug("Starting application GUI.")
    app.run()
