#!/usr/bin/env python3
from tkinter import DISABLED, Frame, Label
from resources import logger
from database import Database
from widgets import TextBox, DatePicker, Selector, Theme, tButton

""" The sign up frame module. """

__author__ = "Thomas Rostrup Andersen"
__copyright__ = """Copyright (C) 2017 Thomas Rostrup Andersen.
        All rights reserved."""
__license__ = "BSD 2-clause"
__version__ = "0.1.8"
__all__ = ["SignUpFrame"]

logger.debug("The sign up frame imported.")


class SignUpFrame(Frame):

    def __init__(self, parent, callback_function=None, *args, **kwargs):
        self.parent = parent
        self.callback_function = callback_function
        super().__init__(parent, *args, **kwargs)
        self.configure(background=Theme.window_background)
        self.columnconfigure(1, weight=4)
        self.columnconfigure(2, weight=1)
        title = "Who are you?"
        self.label = Label(self, text=title)
        self.label.grid(row=0, column=1, columnspan=2, rowspan=1,
                        sticky="wens", padx=(20, 20), pady=(20, 20))
        self.label.configure(state=DISABLED, yscrollcommand=None,
                             background=Theme.window_background,
                             foreground=Theme.text_color, borderwidth=0,
                             highlightthickness=0,
                             font=(Theme.title_font[0], 18))
        self.configure(background=Theme.window_background)
        self.first_name = TextBox(self, hint="First name...", justify="center",
                                  boxed=True)
        self.first_name.grid(row=1, column=1, columnspan=2, rowspan=1,
                             sticky="wen", padx=(20, 20), pady=(20, 2))
        self.last_name = TextBox(self, hint="Last name...", justify="center",
                                 boxed=True, width=10)
        self.last_name.grid(row=2, column=1, columnspan=2, rowspan=1,
                            sticky="wen", padx=(20, 20), pady=(2, 0))
        self.birthday = DatePicker(parent=self, allow_unset=False,
                                   label="Birthday: ", boxed=True)
        self.birthday.grid(row=3, column=1, columnspan=2, rowspan=1,
                           padx=(20, 20), pady=(2, 0))
        self.sex = Selector(parent=self, boxed=True, options=["male", "female"],
                            label="Sex: ")
        self.sex.grid(row=4, column=1, columnspan=2, rowspan=1, padx=(20, 20),
                      pady=(2, 0))
        self.phone_country = Selector(parent=self, boxed=True,
                                      options=["Danmark", "Iceland", "Norway",
                                      "Finland", "Sweeden", "Other"], label="")
        self.phone_country.value = "Norway"
        self.phone_country.grid(row=5, column=1, columnspan=1, rowspan=1,
                                sticky="ens", padx=(20, 20), pady=(2, 0))
        self.phone_number = TextBox(self, hint="Phone number...", boxed=True,
                                    justify="center")
        self.phone_number.grid(row=5, column=2, columnspan=1, rowspan=1,
                               sticky="wns", padx=(0, 20), pady=(2, 0))
        self.email = TextBox(self, hint="E-mail...", boxed=True,
                             justify="center")
        self.email.grid(row=6, column=1, columnspan=2, rowspan=1, sticky="wen",
                        padx=(20, 20), pady=(2, 0))
        self.street_name = TextBox(self, hint="Street...", boxed=True,
                                   width=25, justify="center")
        self.street_name.grid(row=7, column=1, columnspan=2, rowspan=1,
                              sticky="wen", padx=(20, 20), pady=(2, 2))
        self.street_name2 = TextBox(self, hint="Street 2nd line...",
                                    boxed=True, justify="center")
        self.street_name2.grid(row=8, column=1, columnspan=2, rowspan=1,
                               sticky="wen", padx=(20, 20), pady=(2, 2))
        self.code = TextBox(self, hint="Zip Code...", boxed=True, width=10,
                            justify="center")
        self.code.grid(row=9, column=1, columnspan=1, rowspan=1,
                           sticky="ens", padx=(20, 0), pady=(2, 2))
        self.city = TextBox(self, hint="City...", boxed=True, justify="center")
        self.city.grid(row=9, column=2, columnspan=1, rowspan=1, sticky="wns",
                       padx=(5, 20), pady=(2, 2))
        self.state = TextBox(self, hint="State/Province...", boxed=True,
                             justify="center")
        self.state.grid(row=11, column=1, columnspan=2, rowspan=1,
                        sticky="wen", padx=(20, 20), pady=(2, 2))
        self.country = TextBox(self, hint="Country...", boxed=True,
                               justify="center")
        self.country.grid(row=12, column=1, columnspan=2, rowspan=1,
                          sticky="wen", padx=(20, 20), pady=(2, 2))
        self.sign_up_button = tButton(self, label="Continue...",
                                      callback=self.sign_up)
        self.sign_up_button.grid(row=15, column=1, columnspan=2, rowspan=1,
                                 sticky="ns", padx=(20, 20), pady=(30, 10))

    def sign_up(self):
        contact_id = Database.add_contact(first_name=self.first_name.value,
                                          last_name=self.last_name.value,
                                          title="",
                                          birthday=self.birthday.value,
                                          death=None,
                                          sex=self.sex.value, note="",
                                          username=None, groups=None,
                                          organizations=None)
        Database.add_user(contact_id=contact_id)
        try:
            self.callback_function()
        except Exception as e:
            pass

    def change_confirm(self):
        if self.agreed.get() == 1:
            self.sign_up_button.config(state="normal")
        else:
            self.sign_up_button.config(state="disable")
