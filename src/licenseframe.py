#!/usr/bin/env python3
import datetime
from tkinter import DISABLED, Frame, Label
from resources import logger
from applicationdatabase import AppDB
from widgets import Theme, tButton, TextField


""" The license frame module. """

__author__ = "Thomas Rostrup Andersen"
__copyright__ = """Copyright (C) 2017 Thomas Rostrup Andersen.
        All rights reserved."""
__license__ = "BSD 2-clause"
__version__ = "0.1.8"
__all__ = ["LicenseFrame"]

logger.debug("The license frame imported.")


class LicenseFrame(Frame):

    """A license frame. The user must accept the license."""

    def __init__(self, license_id, parent, callback_function=None, *args,
                 **kwargs):
        self.license_id = license_id
        self.parent = parent
        self.callback_function = callback_function
        super().__init__(parent, *args, **kwargs)
        self.record = AppDB.get_license(license_id=self.license_id)[0]
        title = "{} - Do you agree?".format(self.record.name)
        self.label = Label(self, text=title)
        self.label.grid(row=0, column=1, columnspan=1, rowspan=1,
                        sticky="wens", padx=(20, 20), pady=(20, 20))
        self.label.configure(state=DISABLED, yscrollcommand=None,
                             background=Theme.window_background,
                             foreground=Theme.text_color, borderwidth=0,
                             highlightthickness=0, font=Theme.text_font)
        self.configure(background=Theme.window_background)
        self.license = TextField(self, card=True, boxed=True)
        self.license.value = self.record.description
        self.license.grid(row=1, column=1, columnspan=1, rowspan=1,
                          sticky="wen", padx=(20, 20), pady=(5, 20))
        self._accept_button = tButton(self, label="Agree",
                                      callback=self._click_continue)
        self._accept_button.grid(row=2, column=1, columnspan=1, rowspan=1,
                                 sticky="se", padx=(20, 20), pady=(0, 10))

    def _click_continue(self):
        AppDB.update_license(license_id=self.record.license_id,
                name=self.record.name,
                short_name=self.record.short_name,
                description=self.record.description,
                note=self.record.note,
                accepted=True,
                accepted_date=datetime.datetime.now(),
                revision_date=self.record.revision_date,
                filename=self.record.filename)
        if self.callback_function is not None:
            self.callback_function()
