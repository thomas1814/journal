#!/usr/bin/env python3
import datetime
from resources import logger
from tkinter import Label, Toplevel, Frame
from widgets import TextField, TextBox, TimePicker, DatePicker, Theme
from database import Database

""" The entry window module """

__author__ = "Thomas Rostrup Andersen"
__copyright__ = """Copyright (C) 2017 Thomas Rostrup Andersen.
        All rights reserved."""
__license__ = "BSD 2-clause"
__version__ = "0.1.8"
__all__ = ["EntryWindow"]

logger.debug("The entry window module imported.")


class EntryWindow():
    """Create a new entry window for a journal entry.

    Usage:
    * new()
    * load()
    * save()
    """

    def __init__(self, parent, callback_close=None):
        """Create a new window with widgets.

        * parent: Tk parent frame/window.
        * callback_close: a function to be called when window is closed."""
        self.parent = parent
        self.callback_close = callback_close
        self.gui = Toplevel(self.parent)
        self.gui.title("Journal")
        self.gui.geometry("650x750")  # WxH
        self.gui.protocol("WM_DELETE_WINDOW", self._close_window)
        self.gui.protocol("WM_SAVE_YOURSELF", self.save)
        self.gui.configure(background=Theme.window_background)
        self.gui.columnconfigure(0, weight=1)
        self.gui.rowconfigure(0, weight=1)

        border = 3
        self.border = Frame(self.gui)
        self.border.configure(background=Theme.border_color)
        self.border.grid(row=0, column=0, columnspan=2, rowspan=1,
                         padx=(20, 20), pady=(20, 20), sticky="wens")
        self.border.columnconfigure(0, weight=1)
        self.border.rowconfigure(0, weight=1)

        # Allow space between the label and the border.
        self.space = Frame(self.border)
        self.space.configure(background=Theme.card_background)
        self.space.grid(row=0, column=0, columnspan=1, rowspan=1,
                        sticky="wens", padx=(border, border),
                        pady=(border, border))
        self.space.columnconfigure(1, weight=1)

        pad = 60
        self.title = TextBox(self.space, hint="Title...", callback=self.save,
                             card=True, font="title")
        self.title.grid(row=0, column=0, columnspan=2, rowspan=1, sticky="wen",
                        padx=(pad, pad), pady=(pad, 0))
        self.date = DatePicker(self.space, card=True, label="Date: ",
                               callback=self.save)
        self.date.grid(row=1, column=0, columnspan=2, rowspan=1, sticky="wn",
                       padx=(pad, pad), pady=(0, 0))
        self.time = TimePicker(self.space, card=True, label="Time: ",
                               callback=self.save)
        self.time.grid(row=2, column=0, columnspan=2, rowspan=1, sticky="wn",
                       padx=(pad, pad), pady=(0, 0))
        self.location = TextBox(self.space, hint="Location...",
                                callback=self.save, card=True)
        self.location.grid(row=3, column=1, columnspan=1, rowspan=1,
                           sticky="wen", padx=(0, pad), pady=(0, 0))
        self.location_label = Label(self.space, text="Location: ")
        self.location_label.configure(background=Theme.card_background,
                                      borderwidth=0,
                                      foreground=Theme.text_color,
                                      font=Theme.text_font,
                                      highlightthickness=0)
        self.location_label.grid(row=3, column=0, columnspan=1, rowspan=1,
                                 sticky="wn", padx=(pad, 0), pady=(0, 0))
        self.author = TextBox(self.space, hint="Author...", callback=self.save,
                              card=True)
        self.author.grid(row=4, column=1, columnspan=1, rowspan=1,
                         sticky="wen", padx=(0, pad), pady=(0, 0))
        self.author_label = Label(self.space, text="Author: ")
        self.author_label.configure(background=Theme.card_background,
                                    borderwidth=0, foreground=Theme.text_color,
                                    font=Theme.text_font, highlightthickness=0)
        self.author_label.grid(row=4, column=0, columnspan=1, rowspan=1,
                               sticky="wn", padx=(pad, 0), pady=(0, 0))

        border = 0
        self.text = TextField(self.space, callback=self.save,
                              hint="What happend?", card=True)
        self.space.rowconfigure(5, weight=1)
        self.text.grid(row=5, column=0, columnspan=2, rowspan=1, sticky="wens",
                       padx=(pad, pad), pady=(0, pad))
        logger.debug("Entry form created.")

    def disp(self, event):
        pass

    def load(self, entry_id):
        """Load entry with entry_id into the form."""
        self.entry_id = entry_id
        print(self.entry_id)
        record = Database.get_entry(entry_id=self.entry_id)[0]
        self.journal_id = record.journal_id
        self.user_id = record.user_id
        self.title.value = record.entry_title
        self.time.value = record.entry_date.time()
        self.date.value = record.entry_date.date()
        self.location.value = record.location
        self.text.value = record.entry_text
        record = Database.get_user(user_id=self.user_id)[0]
        self.author.value = "{} {}".format(record.first_name, record.last_name)

        # Automatically save edits
        self.title.bind("<KeyRelease>", self.save)
        self.location.bind("<KeyRelease>", self.save)
        self.author.bind("<KeyRelease>", self.save)
        logger.debug("Entry {} loaded.".format(self.entry_id))

    def new(self, journal_id, user_id):
        """Create a new entry in the database and load it on the form."""
        entry_id = Database.add_entry(journal_id=journal_id, location="",
                                      entry_title="", entry_text="",
                                      entry_date=datetime.datetime.now(),
                                      user_id=user_id)
        logger.debug("Entry {} created.".format(entry_id))
        self.load(entry_id=entry_id)

    def save(self, event=None):
        """Save the current values on the form."""
        dt = datetime.datetime.combine(self.date.value, self.time.value)
        entry_text = self.text.value
        status = Database.update_entry(entry_id=self.entry_id,
                                       journal_id=self.journal_id,
                                       location=self.location.value,
                                       entry_title=self.title.value,
                                       entry_text=entry_text,
                                       entry_date=dt, user_id=self.user_id)
        logger.debug("Entry {} updated: {}.".format(self.entry_id, status))

    def _close_window(self):
        """Save the data currently on form, destroy the GUI and call the
        parent`s callback for closing window."""
        self.save()
        self.gui.destroy()
        if self.callback_close is not None:
            self.callback_close()
        logger.debug("Entry {} closed.".format(self.entry_id))
